<style>

</style>
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${contextName}/assets/dist/img/user2-160x160.jpg"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>${username}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control"
					placeholder="Search..."> <span class="input-group-btn">
					<button type="submit" name="search" id="search-btn"
						class="btn btn-flat">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="treeview">
				<a href="#"> 
					<i	class="fa fa-dashboard"></i> <span>Data Master</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/siswa.html" class="menu-item"><i class="fa fa-address-card"></i>Input Data Siswa</a></li>
					<li><a href="${contextName}/guru.html" class="menu-item"><i class="fa fa-graduation-cap"></i>Input Data Guru</a></li>
					<li><a href="${contextName}/mapel.html" class="menu-item"><i class="fa fa-laptop"></i>Input Data Mapel</a></li>
					<li><a href="${contextName}/kelas.html" class="menu-item"><i class="fa fa-institution"></i>Input Data Kelas</a></li>
					<li><a href="${contextName}/semester.html" class="menu-item"><i class="fa fa-pie-chart"></i>Input Data Semester</a></li>
					<li><a href="${contextName}/ta.html" class="menu-item"><i class="fa fa-ticket"></i>Input Data Tahun Ajaran</a></li>
					<li><a href="${contextName}/usersRole.html" class="menu-item"><i class="fa fa-key"></i>Input Data Role User</a></li>
					<li><a href="${contextName}/statusAbsen.html" class="menu-item"><i class="fa fa-sign-in"></i>Input Data Status Absen</a></li>
					<li><a href="${contextName}/ekskul.html" class="menu-item"><i class="fa fa-sign-in"></i>Input Data Ekstrakurikuler</a></li>
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-laptop"></i> <span>Transaksi</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/trxKelas.html" class="menu-item"><i class="fa fa-sitemap"></i>Input Data konfigurasi kelas</a></li>
					<li><a href="${contextName}/trxNilaiHarian.html" class="menu-item"><i class="fa fa-tasks"></i>Input Data Nilai Harian</a></li>
					<li><a href="${contextName}/trxAbsen.html" class="menu-item"><i class="fa fa-tachometer"></i>Input Data Absen Siswa</a></li>
					<li><a href="${contextName}/konfigEkskul.html" class="menu-item"><i class="fa fa-tachometer"></i>Input Data Nilai Ekskul</a></li>
				</ul>
			</li>
			
		<%-- <li>
				<a href="#"> 
					<i	class="fa fa-pie-chart"></i> <span>Transaksi</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/transaksi/adjustment.html"><i class="fa fa-circle-o"></i> Request</a></li>
					<li><a href="${contextName}/transaksi/transfer.html"><i class="fa fa-circle-o"></i> Order</a></li>
				</ul>
			</li> --%>
			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>