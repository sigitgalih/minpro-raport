<style>
.modal-header {
    padding:9px 15px;
    color:white;
    border-bottom:1px solid #eee;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }
</style>
<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Mata Pelajaran</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse"
				data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button>
		</div>
	</div>
	<div id="data-input" class="collapse">
		<form action="" method="post" id="" class="form-horizontal form-mapel">

			<div class="box-body">
				<input type="hidden" name="id" id="" value="1"
					class="form-control id"> <input type="hidden" name="proses"
					id="" value="insert" class="form-control proses">
				<div class="form-group">
					<label class="control-label col-md-2">Kode Mapel</label>
					<div class="col-md-8">
						<input type="text" required name="kdMapel" id="" class="form-control kdMapel"
						oninvalid="this.setCustomValidity('Harus Diisi Angka')" 
						onchange="this.setCustomValidity('')" pattern="^[0-9]*" maxlength="5">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Nama Mata Pelajaran</label>
					<div class="col-md-8">
						<input type="text" required name="nmMapel" id="" class="form-control nmMapel"
						oninvalid="this.setCustomValidity('Nama Mapel harus diisi')" 
						onchange="this.setCustomValidity('')">
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
			</div>
		</form>
	</div>
	<!-- Modal =====================EDIT========================-->
	<div class="modal fade" id="modalEdit" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Mata Pelajaran</h4>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-mapelEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
							<div class="form-group">
								<label class="control-label col-md-2">Kode Mapel</label>
								<div class="col-md-8">
									<input type="text" pattern="^[0-9]*" required name="kdMapel" id="" class="form-control kdMapel"
									oninvalid="this.setCustomValidity('Harus Diisi Angka')" 
									onchange="this.setCustomValidity('')" pattern="^[0-9]*" maxlength="5">
								</div>
							</div>		
							<div class="form-group">
								<label class="control-label col-md-2">Nama Mata Pelajaran</label>
								<div class="col-md-8">
									<input type="text" required name="nmMapel" id="" class="form-control nmMapel"
									oninvalid="this.setCustomValidity('Nama mapel harus diisi')" 
									onchange="this.setCustomValidity('')">
								</div>
							</div>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
					<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
		<!-- Modal =====================HAPUSSS========================-->
	<div class="modal fade" id="modalHapus" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="alert alert-danger has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Peringatan..!!</h4></center>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-mapelHapus">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
							<div class="form-group">
								<label class="control-label col-md-2">Kode Mapel</label>
								<div class="col-md-8">
									<input type="text" name="kdMapel" id="" class="form-control kdMapel" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Nama Mata Pelajaran</label>
								<div class="col-md-8">
									<input type="text" name="nmMapel" id="" class="form-control nmMapel" readonly>
								</div>
							</div>
							<hr>
							<h5>Apakah Anda Yakin Ingin Menghapus Data Diatas</h5>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-danger col-md-2">Hapus</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Mata Pelajaran</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID Mata Pelajaran</th>
					<th>Kode Mata Pelajaran</th>
					<th>Nama Mata Pelajaran</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
	//============================number validation====================
	/*jQuery.validator.setDefaults({
  		debug: true,
  		success: "valid"
	});
	$( ".kdMapel" ).validate({
		rules: {
			field: {
		      required: true,
		      number: true
		    }
		}
	});*/

		
	//============================LOAD DATA============================	
	function loadData() {
		$.ajax({
					url : 'mapel/load.json',
					type : 'get',
					dataType : 'json',
					success : function(hasil) {
						if (hasil.result == 'success') {
							// sebelum menambah data di kosongkan dulu
							$('#list-data').empty();
							// looping data untuk menambahkan data ke list data
							$
									.each(
											hasil.data,
											function(index, item) {
												var items = '<tr>'
														+ '<td>'
														+ item.id
														+ '<td>'
														+ item.kdMapel
														+ '</td>'
														+ '</td>'
														+ '<td>'
														+ item.nmMapel
														+ '</td>'
														+ '<td>'
														+ '<button type="button" data-toggle="modal" data-target="#modalEdit" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
														+ '&nbsp&nbsp<button type="button" data-toggle="modal" data-target="#modalHapus" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
														+ '</td></tr>';

												// menambahkan item ke list data	                
												$('#list-data').append(items);
											});
						}
					}
				});
	}
	// ===================== SUBMIT SIMPAN ================
	$(".form-mapel").submit(function() {
		var kdMapel =$('.kdMapel').val();
		if (parseInt(kdMapel)==0||isNaN(kdMapel)){
			$(".text-warning").show();
			$(".kdMapel").focus();
		} else {
			$.ajax({
				url : 'mapel/save.json',
				type : 'post',
				data : $(this).serialize(),
				dataType : 'json',
				success : function(hasil) {
					$(".text-warning").hide();
					alert(hasil.result);
					if (hasil.result == "success") {
						$(".text-warning").hide();
						$(".form-mapel").trigger('reset');
						$('.proses').val('insert');
						loadData();
					}
				}
			});
		}
		
		return false;
	});
	// ===================== SUBMIT EDIT ================
	$(".form-mapelEdit").submit(function() {
		/*var kdMapelEdit =$('.kdMapel').val();
		if (parseInt(kdMapelEdit)==0||isNaN(kdMapelEdit)){
			$(".text-warning").show();
			$('.kdMapel').focus();
		} else{*/
			$.ajax({
				url : 'mapel/save.json',
				type : 'post',
				data : $(this).serialize(),
				dataType : 'json',
				success : function(hasil) {
					alert(hasil.result);
					if (hasil.result == "success") {
						$(".form-mapelEdit").trigger('reset');
						$('.proses').val('insert');
						loadData();
					}
				}
			});
		/*}*/
		
		return false;
	});
	// ===================== SUBMIT HAPUS ================
	$(".form-mapelHapus").submit(function() {
		var kdMapel =$('.kdMapel').val();
		if (parseInt(kdMapel)==0||isNaN(kdMapel)){
			$(".text-warning").show();
			$(".kdMapel").focus();
		} else{
			$.ajax({
				url : 'mapel/save.json',
				type : 'post',
				data : $(this).serialize(),
				dataType : 'json',
				success : function(hasil) {
					alert(hasil.result);
					loadData();
					if (hasil.result == "success") {
						$(".text-warning").hide();
						$(".form-mapelHapus").trigger('reset');
						$('.proses').val('insert');
					}
				}
			});

		}
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'mapel/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.kdMapel').val(hasil.data.kdMapel);
					$('.nmMapel').val(hasil.data.nmMapel);
					$('.proses').val('update');
					$(".form-mapel").trigger('reset');
				}
			}
		});
	});

	
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'mapel/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.kdMapel').val(hasil.data.kdMapel);
					$('.nmMapel').val(hasil.data.nmMapel);
					$('.proses').val('delete');
					$(".form-mapel").trigger('reset');
				}
			}
		});
	});

	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		loadData();
		$(".text-warning").hide();
	});
</script>
