<style>
.modal-header {
    padding:9px 15px;
    color:white;
    border-bottom:1px solid #eee;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }
</style>
<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form User Role</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse"
				data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button>
		</div>
	</div>
	<div id="data-input" class="collapse">
		<form action="" method="post" id="" class="form-horizontal form-role">

			<div class="box-body">
				<input type="hidden" name="id" id="" value="1"
					class="form-control id"> <input type="hidden" name="proses"
					id="" value="insert" class="form-control proses">
				<div class="form-group">
					<label class="control-label col-md-2">Nama User Role</label>
					<div class="col-md-2">
						<select name="roleName" id="" class="form-control roleName"
						required oninvalid="this.setCustomValidity('Harus Diplih..!')" 
						onchange="this.setCustomValidity('')"> 
							<option value="">==Pilih Role==</option>
							<option value="guru">Guru</option>
							<option value="Admin">User</option>
							<option value="wali">Orang Tua</option>
						</select>
						<span><b class="text-danger">Nama Role Sudah ada ..!</b></span>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
			</div>
		</form>
	</div>
	<!-- Modal =====================EDIT========================-->
	<div class="modal fade" id="modalEdit" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data User Role</h4>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-roleEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
						<div class="form-group">
								<label class="control-label col-md-2">Nama User Role</label>
								<div class="col-md-8">
									<input type="text" name="roleName" id="" class="form-control roleName">
									<span><b class="text-danger">Nama Role Sudah ada ..!</b></span>
								</div>
							</div>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2 ">Simpan</button>
					<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
		<!-- Modal =====================HAPUSSS========================-->
	<div class="modal fade" id="modalHapus" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="alert alert-danger has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Peringatan..!!</h4></center>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-roleEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
							<div class="form-group">
								<label class="control-label col-md-2">Nama User Role</label>
								<div class="col-md-8">
									<input type="text" name="roleName" id="" class="form-control roleName" readonly>
								</div>
							</div>
							<hr>
							<h5>Apakah Anda Yakin Ingin Menghapus Data Diatas</h5>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-danger col-md-2">Hapus</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List User Role</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID User Role</th>
					<th>Nama User Role</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>

	function loadData() {
		$.ajax({
					url : 'usersRole/load.json',
					type : 'get',
					dataType : 'json',
					success : function(hasil) {
						if (hasil.result == 'success') {
							// sebelum menambah data di kosongkan dulu
							$('#list-data').empty();
							// looping data untuk menambahkan data ke list data
							$
									.each(
											hasil.data,
											function(index, item) {
												var items = '<tr>'
														+ '<td>'
														+ item.id
														+ '</td>'
														+ '<td>'
														+ item.roleName
														+ '</td>'
														+ '<td>'
														+ '<button type="button" data-toggle="modal" data-target="#modalEdit" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
														+ '&nbsp&nbsp<button type="button" data-toggle="modal" data-target="#modalHapus" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
														+ '</td></tr>';

												// menambahkan item ke list data	                
												$('#list-data').append(items);
											});
						}
					}
				});
	}
	// ===================== SUBMIT SIMPAN ================
	$(".form-role").submit(function() {
		$.ajax({
			url : 'usersRole/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-role").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT EDIT ================
	$(".form-roleEdit").submit(function() {
		$.ajax({
			url : 'usersRole/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-roleEdit").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT HAPUS ================
	$(".form-roleHapus").submit(function() {
		$.ajax({
			url : 'usersRole/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				loadData();
				if (hasil.result == "success") {
					$(".form-roleHapus").trigger('reset');
					$('.proses').val('insert');
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'usersRole/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.roleName').val(hasil.data.roleName);
					$('.proses').val('update');
					$(".form-role").trigger('reset');
				}
			}
		});
	});

	
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'usersRole/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.roleName').val(hasil.data.roleName);
					$('.proses').val('delete');
					$(".form-role").trigger('reset');
				}
			}
		});
	});

	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		//======= Validating rolee =====
		$('.text-danger').hide();
		$('.roleName').change(function(){
		    var roleName = $('.roleName').val();
			$.ajax({
				url: 'usersRole/getByNamaRole.json',
				type: 'post',
				data: {roleName:roleName},
				dataType: 'json',
				success: function(hasil){
					if (hasil.data == 1) {
						$('.text-danger').show();
						$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-danger').hide();		
					}
				}
			}); 
		});
		
		// ====== validate Keyup ======
			$('.roleName').keyup(function(){
			var roleName = $('.roleName').val();
			$.ajax({
				url: 'usersRole/getByNamaRole.json',
				type: 'post',
				data: {roleName:roleName},
				dataType: 'json',
				success: function(hasil){
					if (hasil.data == 1) {
						$('.text-danger').show();
						$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-danger').hide();		
					}
				}
			});
		});
		
		$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		loadData();
	});
</script>
