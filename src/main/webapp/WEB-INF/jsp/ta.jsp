<style>
.modal-header {
    padding:9px 15px;
    color:white;
    border-bottom:1px solid #eee;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }
</style>
<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">Form Tahun Ajaran</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse"
				data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button>
		</div>
	</div>
	<div id="data-input" class="collapse">
		<form action="" method="post" id="" class="form-horizontal form-tahunAjaran" name="data-ta">

			<div class="box-body">
				<input type="hidden" name="id" id="" value="1"
					class="form-control id"> <input type="hidden" name="proses"
					id="" value="insert" class="form-control proses">

				<div class="form-group">
					<label class="control-label col-md-2">Tahun Ajaran</label>
					<div class="col-md-1">
						<input required="required" type="text" name="tahunAjaran" id="" class="form-control tahunAjaran1" placeholder="yyyy">
					</div>
					<div class="col-md-1">
						<input type="text" name="tahunAjaran" id="" class="form-control tahunAjaran2" placeholder="yyyy" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Tanggal Mulai</label>
					<div class="col-md-2">
						<input type="text" name="tglMulai" id="" class="form-control tglMulai" placeholder="dd-mm-yyyy">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Tanggal Selesai</label>
					<div class="col-md-2">
						<input type="text" name="tglSelesai" id="" class="form-control tglSelesai" placeholder="dd-mm-yyyy">
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
			</div>
		</form>
	</div>
	<!-- Modal =====================EDIT========================-->
	<div class="modal fade" id="modalEdit" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header has-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Tahun Ajaran</h4>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-tahunAjaranEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">

							<div class="form-group">
								<label class="control-label col-md-2">Tahun Ajaran</label>
								<div class="col-md-8">
									<input type="text" name="tahunAjaran" id="" class="form-control tahunAjaran" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2">Tanggal Mulai</label>
								<div class="col-md-8">
									<input type="text" name="tglMulai" id="" class="form-control tglMulai" placeholder="dd-mm-yyyy">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Tanggal Selesai</label>
								<div class="col-md-8">
									<input type="text" name="tglSelesai" id="" class="form-control tglSelesai" placeholder="dd-mm-yyyy">
								</div>
							</div>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
					<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
		<!-- Modal =====================HAPUSSS========================-->
	<div class="modal fade" id="modalHapus" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="alert alert-danger has-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Peringatan..!!</h4></center>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-tahunAjaranEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">

							<div class="form-group">
								<label class="control-label col-md-2">Tahun Ajaran</label>
								<div class="col-md-8">
									<input type="text" name="tahunAjaran" id="" class="form-control tahunAjaran" readonly>
								</div>
							</div>
							<hr>
							<h5>Apakah Anda Yakin Ingin Menghapus Data Diatas</h5>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-danger col-md-2">Hapus</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">List Tahun Ajaran</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive" id="data-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tahun Ajaran</th>
					<th>Tanggal Mulai</th>
					<th>Tanggal Selesai</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>
<script>
	function loadData() {
		$.ajax({
					url : 'ta/load.json',
					type : 'get',
					dataType : 'json',
					success : function(hasil) {
						if (hasil.result == 'success') {
							// sebelum menambah data di kosongkan dulu
							$('#list-data').empty();
							// looping data untuk menambahkan data ke list data
							$
									.each(
											hasil.data,
											function(index, item) {
												var items = '<tr>'
														+ '<td>'
														+ item.id
														+ '</td>'
														+ '<td>'
														+ item.tahunAjaran
														+ '</td>'
														+ '<td>'
														+ item.tglMulai
														+ '</td>'
														+ '<td>'
														+ item.tglSelesai
														+ '</td>'
														+ '<td>'
														+ '<button type="button" data-toggle="modal" data-target="#modalEdit" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
														+ '&nbsp&nbsp<button type="button" data-toggle="modal" data-target="#modalHapus" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
														+ '</td></tr>';

												// menambahkan item ke list data	                
												$('#list-data').append(items);
											});
						}
					}
				});
	}
	// ===================== SUBMIT SIMPAN ================
	$(".form-tahunAjaran").submit(function() {
			$.ajax({
				url : 'ta/save.json',
				type : 'post',
				data : $(this).serialize(),
				dataType : 'json',
				success : function(hasil) {
					alert(hasil.result);
					if (hasil.result == "success") {
						$(".form-tahunAjaran").trigger('reset');
						$('.proses').val('insert');
						loadData();
					}
				}
			});
			return false;	
		});
	// ===================== SUBMIT EDIT ================
	$(".form-tahunAjaranEdit").submit(function() {
		$.ajax({
			url : 'ta/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-tahunAjaranEdit").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT HAPUS ================
	$(".form-tahunAjaranHapus").submit(function() {
		$.ajax({
			url : 'ta/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				loadData();
				if (hasil.result == "success") {
					$(".form-tahunAjaranHapus").trigger('reset');
					$('.proses').val('insert');
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'ta/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.tahunAjaran').val(hasil.data.tahunAjaran);
					$('.tglMulai').val(hasil.data.tglMulai);
					$('.tglSelesai').val(hasil.data.tglSelesai);
					$('.proses').val('update');
					$(".form-tahunAjaran").trigger('reset');
				}
			}
		});
	});

	
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'ta/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.tahunAjaran').val(hasil.data.tahunAjaran);
					$('.tglMulai').val(hasil.data.tglMulai);
					$('.tglSelesai').val(hasil.data.tglSelesai);
					$('.proses').val('delete');
					$(".form-tahunAjaran").trigger('reset');
				}
			}
		});
	});

	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		/* $('#data-table').dataTable({
			"search":{
				"regex": true
			}
		});
		 */
		/* $('form[name="data-ta"]').validate({
			rules:{
				tahunAjaran: "required"
			},
			messages:{
				tahunAjaran: "Harus Diisi"
			},
			submitHandler: function(form) {
		      form.submit();
		    }
		}); */
		
		$(".tahunAjaran1").datepicker({
			autoclose : true,
			viewMode: "years", 
		    minViewMode: "years",
			format : 'yyyy'
		});
		$(".tahunAjaran2").datepicker({
			autoclose : true,
			viewMode: "years", 
		    minViewMode: "years",
			format : 'yyyy'
		});
		
		$(".tahunAjaran1").change(function() {
			$(".tahunAjaran2").focus();
		});
		
		$(".tglMulai").datepicker({
			autoclose : true,
			format : 'dd/mm/yyyy'
		});
		$(".tglSelesai").datepicker({
			autoclose : true,
			format : 'dd/mm/yyyy'
		});
		$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		loadData();
	});
</script>
