<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${data}">
	<tr>
		<td>${item.id}</td>
		<td>${item.nik}</td>
		<td>${item.nama}</td>
		<td>${item.jk}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-edit" value="${item.id }" data-toggle="modal" data-target="#modalEdit"><i class="fa fa-edit"></i></button>
			<button type="button" class="btn btn-danger btn-xs btn-delete" value="${item.id }" data-toggle="modal" data-target="#modalHapus"><i class="fa fa-trash"></i></button>
		</td>
	</tr>
</c:forEach>