<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Users</h3>
	</div>
	<form action="" method="post" id="form-users"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			
			<div class="form-group">
				<label class="control-label col-md-2">Username</label>
				<div class="col-md-8">
					<input type="text" name="usernames" id="usernames" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Password</label>
				<div class="col-md-8">
					<input type="text" name="pass" id="pass"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Role</label>
				<div class="col-md-8">
					<select name="userId" id="userId" class="form-control">
						<option value="">---Pilih Role---</option>
					</select>
				</div>
			</div>
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Users</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID Users</th>
					<th>Username</th>
					<th>Password</th>
					<th>Role</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
	function loadData() {
		$.ajax({
			url : 'users/load.json',
			type : 'get',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#list-data').empty();
					$('#list-detail').empty();
					// looping data untuk menambahkan data ke list data
					$.each(hasil.data,function(index, item) {
						var items = '<tr>'
					 			  + '<td>'+ item.id +'</td>'
								  + '<td>'+ item.usernames+ '</td>'
								  + '<td>'+ item.pass+ '</td>'
								  + '<td>'+ item.usersId+ '</td>'
								  + '<td>'
								       + '<button type="button" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
						     + '&nbsp&nbsp<button type="button" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
								  +'</td></tr>';	
							
						// menambahkan item ke list data	                
						$('#list-data').append(items);
					});		  					
			    }
			}
		});
	}
	
	function loadRole() {
		$.ajax({
			url : 'usersRole/load.json',
			type : 'GET',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#userId').empty();
					$('#userId').append(
							'<option value="">Pilih Role</option>');
					// looping data untuk menambahkan data ke list data
					$.each(hasil.data, function(index, item) {
						$('#userId').append(
								'<option value="'+item.id+'">' + item.roleName
										+ '</option>');
					});
				}
			}
		});
	}	
	$("#form-users").submit(function() {
		$.ajax({
			url : 'users/save.json',
			type : 'POST',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-users").trigger('reset');
					$('#proses').val('insert');
					$('#id').val('');
					loadData();
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'users/getById.json',
			type : 'POST',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#usernames').val(hasil.data.usernames);
					$('#pass').val(hasil.data.pass);
					$('#roleId').val(hasil.data.roleId);
					$('#proses').val('update');
					
				}
			}
		});
	});
    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'users/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#usernames').val(hasil.data.usernames);
					$('#pass').val(hasil.data.pass);
					$('#roleId').val(hasil.data.roleId);
					$('#proses').val('delete');
				}
			}
		});
	});
	
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		loadRole();
		// panggil method loadData, loadSuplier, loadBarang
		loadData();
		
	});
</script>