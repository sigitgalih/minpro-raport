<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Mapel Kelas</h3>
	</div>
	<form action="" method="post" id="form-mapelKelas"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			
			<div class="form-group">
				<label class="control-label col-md-2">Nama Mapel</label>
				<div class="col-md-8">
					<select name="mapelId" id="mapelId" class="form-control">
						<option>--pilih mapel--</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Nama Kelas</label>
				<div class="col-md-8">
					<select name="kelasId" id="kelasId" class="form-control">
						<option>--pilih kelas--</option>
					</select>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Mapel Kelas</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID Mapel Kelas</th>
					<th>Nama Mata Pelajaran</th>
					<th>Nama Kelas</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
function loadMapel() {
	$.ajax({
		url : 'mapel/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#mapelId').empty();
				$('#mapelId').append(
						'<option value="">Pilih Kelas</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#mapelId').append(
							'<option value="'+item.id+'">' + item.nmMapel
									+ '</option>');
				});
			}
		}
	});
}

function loadKelas() {
	$.ajax({
		url : 'kelas/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#kelasId').empty();
				$('#kelasId').append(
						'<option value="">Pilih Kelas</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#kelasId').append(
							'<option value="'+item.id+'">' + item.nmKelas
									+ '</option>');
				});
			}
		}
	});
}
	function loadData() {
		$.ajax({
			url : 'mapelKelas/load.json',
			type : 'get',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#list-data').empty();
					// looping data untuk menambahkan data ke list data
					$.each(hasil.data,function(index, item) {
						var items = '<tr>'
					 			  + '<td>'+ item.id +'</td>'
					 			  + '<td>'+ item.mapel.nmMapel +'</td>'
								  + '<td>'+ item.kelas.nmKelas+ '</td>'
								  + '<td>'
								       + '<button type="button" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
						     + '&nbsp&nbsp<button type="button" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
								  +'</td></tr>';	
							
						// menambahkan item ke list data	                
						$('#list-data').append(items);
					});		  					
			    }
			}
		});
	}

	$("#form-mapelKelas").submit(function() {
		$.ajax({
			url : 'mapelKelas/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-mapelKelas").trigger('reset');
					$('#proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'mapelKelas/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#mapelId').val(hasil.data.mapelId);
					$('#kelasId').val(hasil.data.kelasId);
					$('#proses').val('update');
				}
			}
		});
	});
		

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'mapelKelas/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#mapelId').val(hasil.data.mapelId);
					$('#kelasId').val(hasil.data.kelasId);
					$('#proses').val('delete');
				}
			}
		});
	});
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		loadData();
		loadKelas();
		loadMapel();
	});
</script>
