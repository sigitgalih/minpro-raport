<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Nilai Harian</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse" data-target="#data-input"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div id="data-input" class="collapse">
	     </div>
	
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Data Nilai Harian</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tanggal Ulangan</th>
					<th>Nama Kelas</th>
					<th>Semester</th>
					<th>Mata Pelajaran</th>
					<th>Total Nilai</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
			
		</table>
	</div>
</div>

<script>
// =========================load TRX KELAS ======================
function loadTrxKelas() {
	$.ajax({
		url : 'trxKelas/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#trxKelasId').empty();
				$('#trxKelasId').append(
						'<option value="">==Pilih Kelas==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#trxKelasId').append(
							'<option value="'+item.id+'" data-id="'+item.kelas.nmKelas+'" data-guru="'+item.guru.nmGuru+'" data-ta="'+item.ta.tahunAjaran+'">' +item.id+ '</option>');
				});
				
			}
		}
	});
}

//==================load TRX KELAS DETAIL=========================
function loadTrxKelasDetail() {
	$.ajax({
		url : 'detailNilai/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#trxKelasId').empty();
				$('#trxKelasId').append(
						'<option value="">==Pilih Kelas==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#trxKelasId').append(
							'<option value="'+item.id+'">' +item.id+ '</option>');
				});
			}
		}
	});
}
	
	
//=========================load Semester data ======================
function loadSemester() {
	$.ajax({
		url : 'semester/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#semesterId').empty();
				$('#semesterId').append(
						'<option value="">==Pilih Semester==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#semesterId').append(
							'<option value="'+item.id+'">' + item.nmSemester
									+ '</option>');
				});
			}
		}
	});
}
//=========================load MATA PELAJARAN data ======================
function loadMapel() {
	$.ajax({
		url : 'mapel/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#mapelId').empty();
				$('#mapelId').append(
						'<option value="">==Pilih Mata Pelajaran==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#mapelId').append(
							'<option value="'+item.id+'">' + item.nmMapel
									+ '</option>');
				});
			}
		}
	});
}
	function loadData() {
		$.ajax({
			url : 'trxNilaiHarian/load.html',
			type : 'get',
			dataType : 'html',
			success : function(hasil) {
				$('#list-data').html(hasil);
			}
		});
	}

	$("#form-harian").submit(function() {
		$.ajax({
			url : 'trxNilaiHarian/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-harian").trigger('reset');
					$('#proses').val('insert');
					$('#list-detail').empty();
					loadData();
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'trxNilaiHarian/getById.html',
			type : 'post',
			data : {id:id},
			dataType : 'html',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					
					$('#trxKelasId').val(hasil.data.trxKelasId);
					//$('#kelasId').val(hasil.data.trxKelasId.kelasId);
					//$('#guruId').val(hasil.data.trxKelasId.guruId);
					//$('#taId').val(hasil.data.trxKelasId.taId);
					$('#tglNilai').val(hasil.data.tglNilai);
					$('#semesterId').val(hasil.data.semesterId);
					$('#mapelId').val(hasil.data.mapelId);
					$('#nilaiRata').val(hasil.data.nilaiRata);
					$('#proses').val('update');
					$("#list-detail").empty();	
					// load data detail
					$.each(hasil.data.detailNilai, function(index, item){
					/* 	var dat1 = '<tr id="detail_'+index+'">' +
							'<td><input type="hidden" name="detail['+index+'].barangid" id="detail_'+index+'_barangid" value="'+item.barangid+'"/>' + item.barangid + '</td>'+ 
							'<td><input type="text" name="detail['+index+'].harga" id="detail_'+index+'_harga" value="'+item.harga+'" class="form-control"/></td>' +
							'<td><input type="text" name="detail['+index+'].quantity" id="detail_'+index+'_quantity" value="'+item.quantity+'" class="form-control"/></td>'+
							'<td><input type="text" name="detail['+index+'].subtotal" id="detail_'+index+'_subtotal" value="'+item.subtotal+'" class="form-control"/></td>'+
							'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';			
					 */	
						var dat1 = '<tr id="detailNilai_'+index+'">' +
						'<td><input type="hidden" name="detailNilai['+index+'].siswaId" id="detailNilai_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+  
						'<td class="subtot"><input type="hidden" name="detailNilai['+index+'].nilai" id="detailNilai_'+index+'nilai" value="'+item.nilai+'" class="form-control"/>' + item.nilai + '</td>'+ 
						'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';			
				
						$("#list-detail").append(dat1);	
					});
				}
			}
		});
	});
		

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'trxNilaiHarian/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#trxKelasId').val(hasil.data.trxKelasId);
					$('#tglNilai').val(hasil.data.tglNilai);
					$('#semesterId').val(hasil.data.semesterId);
					$('#mapelId').val(hasil.data.mapelId);
					$('#nilaiRata').val(hasil.data.nilaiRata);
					$('#proses').val('delete');
					$("#list-detail").empty();
					// load data detail
					$.each(hasil.data.detailNilai, function(index, item){
					/* 	var dat1 = '<tr id="detail_'+index+'">' +
							'<td><input type="hidden" name="detail['+index+'].barangid" id="detail_'+index+'_barangid" value="'+item.barangid+'"/>' + item.barangid + '</td>'+ 
							'<td><input type="text" name="detail['+index+'].harga" id="detail_'+index+'_harga" value="'+item.harga+'" class="form-control"/></td>' +
							'<td><input type="text" name="detail['+index+'].quantity" id="detail_'+index+'_quantity" value="'+item.quantity+'" class="form-control"/></td>'+
							'<td><input type="text" name="detail['+index+'].subtotal" id="detail_'+index+'_subtotal" value="'+item.subtotal+'" class="form-control"/></td>'+
							'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';			
					 */	
						var dat1 = '<tr id="detailNilai_'+index+'">' +
						'<td><input type="hidden" name="detailNilai['+index+'].siswaId" id="detailNilai_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswaId + '</td>'+ 
						'<td class="subtot"><input type="hidden" name="detailNilai['+index+'].nilai" id="detailNilai_'+index+'nilai" value="'+item.nilai+'" class="form-control"/>' + item.nilai + '</td>'+ 
						'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';			
				
						$("#list-detail").append(dat1);	
					});
				}
			}
		});
	});
	
	function loadSiswa() {
		$.ajax({
			url : 'siswa/load.json',
			type : 'GET',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#siswaId').empty();
					$("#siswaId").append('<option value="">== Pilih Siswa ==</option>');
					$.each(hasil.data, function(index, item){
						$('#siswaId').append('<option value="'+item.id+'">'+item.nmSiswa+'</option>');
					});
			    }
			}
		});
	}
		
	
	$('#btn-add-detail').click(function() {
		var siswaId=$('#siswaId').val();
		var nmSiswa = $("#siswaId :selected").text();
		
		var nilai=$('#nilai').val();
		var index=$('#list-detail > tr').length;
		var data='<tr id="detailNilai'+index+'">'+
		  '<td><input type="hidden" name="detailNilai['+index+'].siswaId" id="detailNilai['+index+'].siswaId" value="'+siswaId+'" class="form-control" />'+nmSiswa+'</td>'+
		  '<td class="subtot"><input type="hidden" name="detailNilai['+index+'].nilai" id="detailNilai['+index+'].nilai" value="'+nilai+'" class="form-control" />'+nilai+'</td>'+
		  '<td><button type="button" class="btn btn-danger btn-delete-detail" value="'+index+'" ><i class="fa fa-remove"></i></button></td>'
		  '</tr>';
	  $('#list-detail').append(data);
	  var rata = $("#nilaiRata").val();
	  rata = parseInt(rata) + parseInt(nilai);
	  $("#nilaiRata").val(rata);
	  
	});
	
	$("#list-detail").on("click", ".btn-delete-detail", function() {

		var total = $("#nilaiRata").val(); 
		var sub = $(this).closest('tr').find(".subtot").text(); 
		var hasil = parseInt(total)-parseInt(sub);
		$("#nilaiRata").val(hasil);
		$(this).parent().parent().remove();
		
		//looping tr
		$.each($('#list-detail >tr'), function(index, item){
			var indexLama = $(this).attr('id').match(/\d+/);
			var trBaru = $(this).attr('id').replace('_'+indexLama,'_'+index);
			$(this).attr('id',trBaru);		
					
			//looping td
			$.each($(this).find('.form-control'),function(key, val){
				var namaLama = $(this).attr('name');
				var namaBaru = namaLama.replace('['+indexLama+']','['+index+']');
				$(this).attr('name', namaBaru);
				
				var idLama = $(this).attr('id');
				var idBaru = idLama.replace('_'+indexLama+'_','_'+index+'_');
				$(this).attr('id', idBaru);
			});	
		});
		
	});
	
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		
		// ambil data trxKelas 
		$("#trxKelasId").change(function() {
		var kelas = $(this).find(':selected').attr("data-id");
		var guru = $(this).find(':selected').attr("data-guru");
		var ta = $(this).find(':selected').attr("data-ta");
		$("#kelasId").val(kelas);
		$("#guruId").val(guru);
		$("#taId").val(ta);
	});
		$("#siswaId").change(function() {
			$("#nilai").focus();
		});
		/* $("#tglNilai").change(function() {
			$("#nilai").focus();
		}); */
		$("#tglNilai").datepicker({
			autoclose:true,
			format:'dd-mm-yyyy'
		});
		loadSiswa();
		loadTrxKelas();
		loadSemester();
		loadMapel();
		loadData();
	});
</script>
