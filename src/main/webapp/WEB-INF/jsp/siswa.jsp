<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js">
<!--

//-->
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js">
<!--

//-->
</script>
<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Siswa</h3>
	</div>
	<form action="" method="post" id="form-siswa" name="form-siswa"
		class="form-horizontal">
	<fieldset>
		<legend>Data Siswa</legend>
	</fieldset>
		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control"/>
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control"/>
			
			<div class="form-group">
				<label class="control-label col-md-2">Nama Siswa :</label>
				<div class="col-md-8">
					<input type="text" required name="nmSiswa" id="nmSiswa" class="form-control" placeholder="Masukkan Nama Siswa"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">NISN :</label>
				<div class="col-md-8">
					<input type="text" required name="nisn" id="nisn" class="form-control" placeholder="Masukkan NISN" maxlength="10"/>
					<span><b class="text-warning" style="color:red;">NISN Sudah Terdaftar!</b></span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Jenis Kelamin</label>
				<div class="col-md-3">
					<select id="jk" required name="jk" class="form-control">
						<option value="">==Pilih Jenis Kelamin ==</option>
						<option value="pria">Pria</option>
						<option value="wanita">Wanita</option>
					</select>
				</div>
			</div> 
			<div class="form-group">
				<label class="control-label col-md-2">Tempat/Tanggal Lahir :</label>
				<div class="col-md-5">
					<input type="text" required name="tempat" id="tempat" class="form-control" placeholder="Tempat Lahir"/>
				</div>
				<div class="col-md-3">
					<input type="text" required name="tglLahir" id="tglLahir" class="form-control" placeholder="Tanggal lahir"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat Tinggal :</label>
				<div class="col-md-8">
					<textarea type="text" required name="alamat" id="alamat"
						class="form-control" placeholder="Masukkan Alamat Tinggal"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No. Telepon :</label>
				<div class="col-md-8">
					<input type="text" required name="noTelp" id="noTelp" class="form-control" placeholder="Masukkan No Telepon" maxlength="13" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Jumlah Saudara :</label>
				<div class="col-md-1">
					<input type="text" required name="jmlSaudara" id="jmlSaudara" class="form-control" /> 
				</div>
				<div>
					<label class="control-label col-md-1">bersaudara</label>
				</div>
			</div>
		</div>
		
		<fieldset>
		<legend>Data Orang Tua</legend>
		
		</fieldset>
		
		<div>
			<legend>A. Ayah</legend>
			<div class="form-group">
				<label class="control-label col-md-2">Nama Ayah :</label>
				<div class="col-md-8">
					<input type="text" required name="nmAyah" id="nmAyah" class="form-control" placeholder="Masukkan Nama Ayah"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Telp Ayah :</label>
				<div class="col-md-8">
					<input type="text" required name="noTelpAyah" id="noTelpAyah" class="form-control" placeholder="Masukkan No Telp Ayah" maxlength="13"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Pekerjaan Ayah :</label>
				<div class="col-md-8">
					<input type="text" required name="pekerjaanAyah" id="pekerjaanAyah" class="form-control" placeholder="Masukkan Pekerjaan Ayah"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat Ayah :</label>
				<div class="col-md-8">
					<textarea type="text" required name="alamatAyah" id="alamatAyah"
						class="form-control" placeholder="Masukkan Alamat Ayah"></textarea>
				</div>
			</div>
			
			<legend>B. Ibu</legend>
			<div class="form-group">
				<label class="control-label col-md-2">Nama Ibu :</label>
				<div class="col-md-8">
					<input type="text" required name="nmIbu" id="nmIbu" class="form-control" placeholder="Masukkan Nama Ibu"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Telp Ibu :</label>
				<div class="col-md-8">
					<input type="text" required name="noTelpIbu" id="noTelpIbu" class="form-control" placeholder="Masukkan No Telp Ibu" maxlength="13"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Pekerjaan Ibu :</label>
				<div class="col-md-8">
					<input type="text" required name="pekerjaanIbu" id="pekerjaanIbu" class="form-control" placeholder="Masukkan Pekerjaan Ibu"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat Ibu :</label>
				<div class="col-md-8">
					<textarea type="text" required name="alamatIbu" id="alamatIbu"
						class="form-control" placeholder="Masukkan Alamat Ibu"></textarea>
				</div>
			</div>
			
		</div>
		
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Siswa</h3>
	</div>
	<div class="box-body table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>ID Siswa</th>
					<th>NISN</th>
					<th>Nama Siswa</th>
					<th>Jenis Kelamin</th>
					<th>Tempat Lahir</th>
					<th>Tanggal Lahir</th>
					<th>Alamat</th>
					<th>Telepon Siswa</th>
					<th>Jml Saudara</th>
					<th>Nama Ayah</th>
					<th>No Telp Ayah</th>
					<th>Pekerjaan Ayah</th>
					<th>Alamat Ayah</th>
					<th>Nama Ibu</th>
					<th>No Telp Ibu</th>
					<th>Pekerjaan Ibu</th>
					<th>Alamat Ibu</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
	function loadData() {
		$.ajax({
			url : 'siswa/load.json',
			type : 'get',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#list-data').empty();
					// looping data untuk menambahkan data ke list data
					$.each(hasil.data,function(index, item) {
						var items = '<tr>'
					 			  + '<td>'+ item.id +'</td>'
					 			  + '<td>'+ item.nisn +'</td>'
								  + '<td>'+ item.nmSiswa+ '</td>'
								  + '<td>'+ item.jk+ '</td>'
								  + '<td>'+ item.tempat+ '</td>'
								  + '<td>'+ item.tglLahir+ '</td>'
								  + '<td>'+ item.alamat+ '</td>'
								  + '<td>'+ item.noTelp+ '</td>'
								  + '<td>'+ item.jmlSaudara+ '</td>'
								  + '<td>'+ item.nmAyah+ '</td>'
								  + '<td>'+ item.noTelpAyah+ '</td>'
								  + '<td>'+ item.pekerjaanAyah+ '</td>'
								  + '<td>'+ item.alamatAyah+ '</td>'
								  + '<td>'+ item.nmIbu+ '</td>'
								  + '<td>'+ item.noTelpIbu+ '</td>'
								  + '<td>'+ item.pekerjaanIbu+ '</td>'
								  + '<td>'+ item.alamatIbu+ '</td>'
								  + '<td>'
								       + '<button type="button" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
						     + '&nbsp&nbsp<button type="button" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
								  +'</td></tr>';	
							
						// menambahkan item ke list data	                
						$('#list-data').append(items);
					});		  					
			    }
			}
		});
	}

	$("#form-siswa").submit(function() {
		/*var data = $(".nisn").val();
		if(parseInt(data)==0||isNaN(data)){
			$(".nisn").focus();
			$(".text-number").show();
		} else{*/
			$.ajax({
				url : 'siswa/save.json',
				type : 'post',
				data : $(this).serialize(),
				dataType : 'json',
				success : function(hasil) {
					alert(hasil.result);
					if (hasil.result == "success") {
						$(".text-number").hide();
						$("#form-siswa").trigger('reset');
						$('#proses').val('insert');
						loadData();
					}
				}
			});
		//}
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'siswa/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#nisn').val(hasil.data.nisn);
					$('#nmSiswa').val(hasil.data.nmSiswa);
					$('#username').val(hasil.data.username);
					$('#jk').val(hasil.data.jk);
					$('#tempat').val(hasil.data.tempat);
					$('#tglLahir').val(hasil.data.tglLahir);
					$('#alamat').val(hasil.data.alamat);
					$('#noTelp').val(hasil.data.noTelp);
					$('#jmlSaudara').val(hasil.data.jmlSaudara);
					$('#nmAyah').val(hasil.data.nmAyah);
					$('#noTelpAyah').val(hasil.data.noTelpAyah);
					$('#pekerjaanAyah').val(hasil.data.pekerjaanAyah);
					$('#alamatAyah').val(hasil.data.alamatAyah);
					$('#nmIbu').val(hasil.data.nmIbu);
					$('#noTelpIbu').val(hasil.data.noTelpIbu);
					$('#pekerjaanIbu').val(hasil.data.pekerjaanIbu);
					$('#alamatIbu').val(hasil.data.alamatIbu);
					$('#proses').val('update');
				}
			}
		});
	});
		

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'siswa/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#nisn').val(hasil.data.nisn);
					$('#nmSiswa').val(hasil.data.nmSiswa);
					$('#username').val(hasil.data.username);
					$('#jk').val(hasil.data.jk);
					$('#tempat').val(hasil.data.tempat);
					$('#tglLahir').val(hasil.data.tglLahir);
					$('#alamat').val(hasil.data.alamat);
					$('#noTelp').val(hasil.data.noTelp);
					$('#jmlSaudara').val(hasil.data.jmlSaudara);
					$('#nmAyah').val(hasil.data.nmAyah);
					$('#noTelpAyah').val(hasil.data.noTelpAyah);
					$('#pekerjaanAyah').val(hasil.data.pekerjaanAyah);
					$('#alamatAyah').val(hasil.data.alamatAyah);
					$('#nmIbu').val(hasil.data.nmIbu);
					$('#noTelpIbu').val(hasil.data.noTelpIbu);
					$('#pekerjaanIbu').val(hasil.data.pekerjaanIbu);
					$('#alamatIbu').val(hasil.data.alamatIbu);
					$('#proses').val('delete');
				}
			}
		});
	});
	

	//======================VALIDASI NISN=========================
	$('.nisn').keyup(function(){
		var nisn = $('.nisn').val();
		$.ajax({
			url: 'siswa/getByNisn.json',
			type: 'post',
			data: {nisn:nisn},
			dataType: 'json',
			success: function(hasil){	
				if (hasil.data == 1){
					$('.text-warning').show();
					$(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$('.text-warning').hide();
					$(':input[type="submit"]').prop('disabled', false);
			}
		}
		});
	});

//===================Validasi FORM===============
	/* $(function(){
		$("form[name='form-siswa']").validate({
			alert("serah makan");
			rules:{
				nmSiswa:"required",
				nisn:"required",
				pass: {
					required:true,
					minlength:5
				}
			},
			messages:{
				nmSiswa:"Nama wajib diisi",
				nisn:"NISN wajib diisi",
				pass:{
					required:"Password wajib diisi",
					minlength:"Password minimal 5 karakter"
				}
			},
			submitHandler:function(form){
				form.submit();
			}
		});
	}); */
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		$('.text-warning').hide();
		$(".text-number").hide();
		$("#tglLahir").datepicker({
			autoclose : true,
			format : 'dd/mm/yyyy'
		});
		// panggil method loadData$('.text-warning').hide();
	$('.text-warning').hide();
	
		
		loadData();
		
	});
	
//=====================validator===============
	/*$(function() {
        $("form[name='form-siswa']").validate({
            rules: {
            	nmSiswa: {
                    required: true
                },
                nisn: {
                    required: true,
                    number: true,
                    maxlength: 10
                },
                jk: {
                    required: true
                },
                tempat: {
                    required: true
                },
                tglLahir: {
                    required: true
                },
                alamat: {
                    required: true
                },
                noTelp: {
                    required: true,
                    number: true
                },
                jmlSaudara: {
                    required: true,
                    number: true
                },
                username: {
                    required: true
                },
                password: {
                    required: true,
                    pwcheck: true,
                    minlength: 6
                },
                nmAyah: {
                    required: true
                },
                noTelpAyah: {
                    required: true,
                    number: true
                },
                pekerjaanAyah: {
                    required: true
                },
                alamatAyah: {
                    required: true
                },
                nmIbu: {
                    required: true
                },
                noTelpIbu: {
                    required: true,
                    number: true
                },
                pekerjaanIbu: {
                    required: true
                },
                alamatIbu: {
                    required: true
                }
            },
            messages: {
            	nmSiswa: {
                    required: "Nama harus diisi"
                },
                nisn: {
                    required: "NISN harus diisi",
                    number: "NISN hanya boleh diisi angka",
                    maxlength: "NISN maksimal 10 digit"
                },
                jk: {
                    required: "Jenis Kelamin harus dipilih"
                },
                tempat: {
                    required: "Tempat lahir harus diisi"
                },
                tglLahir: {
                    required: "Tanggal lahir harus diisi"
                },
                alamat: {
                    required: "Alamat harus diisi"
                },
                noTelp: {
                    required: "No Telepon harus diisi",
                    number: "No Telepon harus angka"
                },
                jmlSaudara: {
                    required: "Jumlah saudara harus diisi",
             		number: "Harus angka"
                },
                username: {
                    required: "Username harus diisi"
                },
                password: {
                    required: "Password harus diisi",
                    pwcheck: "Password harus terdiri dari huruf, karakter dan angka",
                    minlength: "Password minimal 6 karakter!"
                },
                nmAyah: {
                    required: "Nama ayah harus diisi"
                },
                noTelpAyah: {
                    required: "No telepon ayah harus diisi",
                    number: "No telepon harus angka"
                },
                pekerjaanAyah: {
                    required: "Pekerjaan ayah harus diisi"
                },
                alamatAyah: {
                    required: "Alamat ayah harus diisi"
                },
                nmIbu: {
                    required: "Nama ibu harus diisi"
                },
                noTelpIbu: {
                    required: "No Telepon harus diisi",
                    number: "No telepon harus diisi"
                },
                pekerjaanIbu: {
                    required: "Pekerjaan ibu harus diisi"
                },
                alamatIbu: {
                    required: "Alamat ibu harus diisi"
                },
                submitHandler: function(form) {
                    form.submit();
                }
            }
        });

        $.validator.addMethod("pwcheck", function(value, element) {
             return /^[A-Za-z0-9\d=!\-@._*]+$/.test(value);
        });
        
    }); */
</script>
