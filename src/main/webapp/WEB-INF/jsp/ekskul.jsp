<style>
.modal-header {
    padding:9px 15px;
    color:white;
    border-bottom:1px solid #eee;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }
</style>
<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">Form Ekstrakurikuler</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse"
				data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button>
		</div>
	</div>
	<div id="data-input" class="collapse">
		<form action="" method="post" id="" class="form-horizontal form-Ekskul">

			<div class="box-body">
					<input type="hidden" name="id" id="" value="1"
					class="form-control id"> <input type="hidden" name="proses"
					id="" value="insert" class="form-control proses">

				<div class="form-group">
					<label class="control-label col-md-2">Nama Ekstrakurikuler :</label>
					<div class="col-md-4">
						<input type="text" name="nmEkskul" id="" class="form-control nmEkskul"
						required oninvalid="this.setCustomValidity('Ekskul Harus Diisi..!')" 
						onchange="this.setCustomValidity('')">
						<span><b class="text-danger">Nama Ekskul Sudah ada ..!</b></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Deskripsi :</label>
					<div class="col-md-8">
						<textarea name="deskripsi" id="" class="form-control deskripsi" 
						required oninvalid="this.setCustomValidity('Deskripsi Harus Diisi..!')" 
						onchange="this.setCustomValidity('')"></textarea>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
			</div>
		</form>
	</div>
	<!-- Modal =====================EDIT========================-->
	<div class="modal fade" id="modalEdit" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header has-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Ekstrakurikuler</h4>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-EkskulEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
							<div class="form-group">
								<label class="control-label col-md-2">Nama Ekstrakurikuler :</label>
								<div class="col-md-8">
									<input type="text" name="nmEkskul" id="" class="form-control nmEkskul">
									<span><b class="text-danger">Nama Ekskul Sudah ada ..!</b></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Deskripsi :</label>
								<div class="col-md-8">
									<textarea name="deskripsi" id="" class="form-control deskripsi"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
							<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
		<!-- Modal =====================HAPUSSS========================-->
	<div class="modal fade" id="modalHapus" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="alert alert-danger has-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Peringatan..!!</h4></center>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-EkskulHapus">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">

							<div class="form-group">
								<label class="control-label col-md-2">Nama Ekstrakurikuler :</label>
								<div class="col-md-8">
									<input type="text" name="nmEkskul" id="" class="form-control nmEkskul">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Deskripsi :</label>
								<div class="col-md-8">
									<textarea name="deskripsi" id="" class="form-control deskripsi"></textarea>
								</div>
							</div>
							
							<hr>
							<h5>Apakah Anda Yakin Ingin Menghapus Data Diatas?</h5>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-danger col-md-2">Hapus</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">List Ekstrakurikuler</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive" id="data-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama Ekskul</th>
					<th>Deskripsi</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
	function loadData() {
		$.ajax({
					url : 'ekskul/load.json',
					type : 'get',
					dataType : 'json',
					success : function(hasil) {
						if (hasil.result == 'success') {
							// sebelum menambah data di kosongkan dulu
							$('#list-data').empty();
							// looping data untuk menambahkan data ke list data
							$
									.each(
											hasil.data,
											function(index, item) {
												var items = '<tr>'
														+ '<td>'
														+ item.id
														+ '</td>'
														+ '<td>'
														+ item.nmEkskul
														+ '</td>'
														+ '<td>'
														+ item.deskripsi
														+ '</td>'
														+ '<td>'
														+ '<button type="button" data-toggle="modal" data-target="#modalEdit" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
														+ '&nbsp&nbsp<button type="button" data-toggle="modal" data-target="#modalHapus" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
														+ '</td></tr>';

												// menambahkan item ke list data	                
												$('#list-data').append(items);
											});
						}
					}
				});
	}
	// ===================== SUBMIT SIMPAN ================
	$(".form-Ekskul").submit(function() {
		$.ajax({
			url : 'ekskul/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-Ekskul").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT EDIT ================
	$(".form-EkskulEdit").submit(function() {
		$.ajax({
			url : 'ekskul/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-EkskulEdit").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT HAPUS ================
	$(".form-EkskulHapus").submit(function() {
		$.ajax({
			url : 'ekskul/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				loadData();
				if (hasil.result == "success") {
					$(".form-EkskulHapus").trigger('reset');
					$('.proses').val('insert');
				}
			}
		});
		return false;
	});
	

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'ekskul/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.nmEkskul').val(hasil.data.nmEkskul);
					$('.deskripsi').val(hasil.data.deskripsi);
					$('.proses').val('update');
					$(".form-Ekskul").trigger('reset');
				}
			}
		});
	});

	
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'ekskul/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.nmEkskul').val(hasil.data.nmEkskul);
					$('.deskripsi').val(hasil.data.deskripsi);
					$('.proses').val('delete');
					$(".form-Ekskul").trigger('reset');
				}
			}
		});
	});
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		// ========= validation Ekskul ======
		$('.text-danger').hide();
		$('.nmEkskul').keyup(function(){
			var nmEkskul = $('.nmEkskul').val();
			$.ajax({
				url: 'ekskul/getByNamaEkskul.json',
				type: 'post',
				data: {nmEkskul:nmEkskul},
				dataType: 'json',
				success: function(hasil){
					if (hasil.data == 1) {
						$('.text-danger').show();
						$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-danger').hide();
						//$(':input[type="submit"]').prop('disabled', true);
					}
				}
			});
		});
		
		
		//================================
		$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		
		
		loadData();
	});
</script>
