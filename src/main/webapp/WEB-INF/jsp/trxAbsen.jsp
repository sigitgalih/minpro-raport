<style>
	hr{
		border: dashed 1px red;
	}
</style>
<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">Form Absen Harian</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse" data-target="#data-input"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div id="data-input" class="collapse">
	<form action="" method="post" id="form-harian"
		class="form-horizontal"> 

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			
			<div class="form-group">
				<label class="control-label col-md-2">Input Tanggal : </label>
					<div class="col-md-2 input-group">
					<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
						<input type="text" name="tglAbsen" id="tglAbsen" class="form-control" placeholder="Masukkan Tanggal"/>
					
					<select name="absenId" id="absenId" class="form-control">
						<option value="">== Piih Absensi ==</option>
					</select>	
				</div>
			</div>
			<hr>
			
			
			<!-- Form Detail Kelas -->
			<div>
			<!--================DETAILNYA================== -->
				<div class="form-group">
					<center><h3>Detail Absen Harian</h3></center>
					<hr>
					<div class="form-group">
				<label class="control-label col-md-2">Pilih Kelas : </label>
				<div class="col-md-2">
					<select name="trxKelasId" id="trxKelasId" class="form-control">
						<option value="">== Piih Kelas ==</option>
					</select>
				</div>
				<div class="col-md-2">
						<select name="siswaId" id="siswaId" class="form-control" required>
							<option value="">== Pilih Siswa ==</option>
						</select>
					</div>	
						<input type="hidden" name="absen" id="absen" class="form-control" value="1"/>
					<div class="col-md-1">
					<button type="button" id="btn-add-detail"  class="btn btn-success btn-add-detail"><i class="fa fa-plus"></i></button>
				</div>
					
				</div>
				
			</div>
				
			</div>
			
			<!-- Tabel detail siswa per kelas -->
			<div class="form-group">
					<table class="table table-responsive" id="data-retail-edit">
						<thead>
							<tr>
								<th class="">Kelas</th>
								<th class="">Nama Siswa</th>
								<th class="">Absen</th>
							</tr>
						</thead>
						<tbody id="list-detail">
						</tbody>
					</table>
			</div>
			<div class="form-group">
				<label class="control-label col-md-9">TOTAL ABSENSI</label>
				<div class="col-md-1">
					<input readonly type="text" name="totalAbsen" id="totalAbsen" class="form-control" value="0">
				</div>
			</div>
		</div>

			
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
     </div>
	
</div>

<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">List Data Absen Harian</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive table-striped table-bordered" id="data-absen">
			<thead class="has-error">
				<tr>
					<th>ID</th>
					<th>Tanggal Absen</th>
					<th>Keterangan</th>
					<th>Jumlah Absen</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
			
		</table>
	</div>
</div>

<script>
//=== variable global ==
	var subtot =0;
	var total = 0;
// =========================load guru ======================
/* function loadGuru() {
	$.ajax({
		url : 'guru/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#guruId').empty();
				$('#guruId').append(
						'<option value="">==Pilih Guru==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#guruId').append(
							'<option value="'+item.id+'">' +item.nama+ '</option>');
				});
			}
		}
	});
} */
// ================= LOAD KELAS ===========================
function loadKelas() {
	$.ajax({
		url : 'trxKelas/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#trxKelasId').empty();
				$('#trxKelasId').append(
						'<option value="0">==Pilih Kelas==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#trxKelasId').append(
							'<option value="'+item.id+'" data-guru="'+item.guru.nama+'" data-ta="'+item.ta.tahunAjaran+'">' +item.kelas.nama+ '</option>');
				});
			}
		}
	});
}

//==================load TA=========================
function loadTa() {
	$.ajax({
		url : 'ta/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#taId').empty();
				$('#taId').append(
						'<option value="">==Pilih Tahun Ajaran==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#taId').append(
							'<option value="'+item.id+'">' +item.tahunAjaran+ '</option>');
				});
			}
		}
	});
}
	
	

//=========================load Jenis absensi data ======================
function loadAbsen() {
	$.ajax({
		url : 'statusAbsen/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#absenId').empty();
				$('#absenId').append(
						'<option value="">==Pilih Absen ==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#absenId').append(
							'<option value="'+item.id+'">' + item.keterangan
									+ '</option>');
				});
			}
		}
	});
} 
	function loadData() {
		$.ajax({
			url : 'trxAbsen/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data").html(data);
			}
		});
	}

	$("#form-harian").submit(function() {
		$.ajax({
			url : 'trxAbsen/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-harian").trigger('reset');
					$('#proses').val('insert');
					$('#list-detail').empty();
					loadData();
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'trxAbsen/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#tglAbsen').datepicker('update', hasil.data.tglAbsen);
					$('#absenId').val(hasil.data.absenId);
					$('#totalAbsen').val(hasil.data.totalAbsen);
					$('#proses').val('update');
					$("#list-detail").empty();	
					// load data detail
					$.each(hasil.data.absenDetail, function(index, item){
				
							var dat1 = '<tr id="absenDetail_'+index+'">' +
							'<td><input type="hidden" name="absenDetail['+index+'].trxKelasId" id="absenDetail_'+index+'_trxKelasId" value="'+item.trxKelasId+'" class="form-control"/>' + item.kelas.kelas.nama + '</td>'+ 
							'<td><input type="hidden" name="absenDetail['+index+'].siswaId" id="absenDetail_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+ 
							'<td class="subtot"><input type="hidden" name="absenDetail['+index+'].absen" id="absenDetail_'+index+'absen" value="'+item.absen+'" class="form-control col-md-1"/>' + item.absen + '</td>'+ 
							'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';			
					
							$("#list-detail").append(dat1);	
						});
					
				}
			}
		});
	});
		

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'trxAbsen/getById.json',
			format: 'dd',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#absenId').val(hasil.data.absenId);
					$('#totalAbsen').val(hasil.data.totalAbsen);
					$('#proses').val('delete');
					$("#list-detail").empty();	
					// load data detail
					$.each(hasil.data.absenDetail, function(index, item){
				
							var dat1 = '<tr id="absenDetail_'+index+'">' +
							'<td><input type="hidden" name="absenDetail['+index+'].trxKelasId" id="absenDetail_'+index+'_trxKelasId" value="'+item.trxKelasId+'" class="form-control"/>' + item.kelas.kelas.nama + '</td>'+ 
							'<td><input type="hidden" name="absenDetail['+index+'].siswaId" id="absenDetail_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+ 
							'<td><input type="hidden" name="absenDetail['+index+'].absen" id="absenDetail_'+index+'absen" value="'+item.absen+'" class="form-control col-md-1" />' + item.absen + '</td>'+ 
							'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';			
					
							$("#list-detail").append(dat1);	
						});
				}
			}
		});
	});
	
	/* function loadSiswa() {
		$.ajax({
			url : 'siswa/load.json',
			type : 'GET',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#siswaId').empty();
					$("#siswaId").append('<option value="">== Pilih Siswa ==</option>');
					$.each(hasil.data, function(index, item){
						$('#siswaId').append('<option value="'+item.id+'">'+item.nmSiswa+'</option>');
					});
			    }
			}
		});
	} */
		
	
	$('#btn-add-detail').click(function() {
		var siswaId=$('#siswaId').val();
		var trxKelasId = $('#trxKelasId').val();
		var kelas = $('#trxKelasId :selected').text();
		var nmSiswa = $("#siswaId :selected").text();
		var tglAbsen=$('#tglAbsen').val();
		var absen=$('#absen').val();
		var index=$('#list-detail > tr').length;
		var data='<tr id="absenDetail'+index+'">'+
		  '<td><input type="hidden" name="absenDetail['+index+'].siswaId" id="absenDetail['+index+'].siswaId" value="'+siswaId+'" class="form-control" />'+nmSiswa+'</td>'+
		  '<td><input type="hidden" name="absenDetail['+index+'].trxKelasId" id="absenDetail['+index+'].trxKelasId" value="'+trxKelasId+'" class="form-control" />'+kelas+'</td>'+
		  '<td><input type="hidden" name="absenDetail['+index+'].absen" id="absenDetail['+index+'].absen" value="'+absen+'" class="form-control col-md-1" />'+absen+'</td>'+
		  '<td><button type="button" class="btn btn-danger btn-delete-detail" value="'+index+'" ><i class="fa fa-remove"></i></button></td>'
		  '</tr>';
	  $('#list-detail').append(data);
	  var total = $("#totalAbsen").val();
	  
	  total = parseInt(total) + parseInt(absen);
		$("#totalAbsen").val(total);
	  
	});
	
	$("#list-detail").on("click", ".btn-delete-detail", function() {
		var total = $("#totalAbsen").val(); 
		total =0;
		var sub = $(this).closest('tr').find(".subtot").text(); 
		var hasil = parseInt(total)-parseInt(sub);
		$("#totalAbsen").val(hasil);
		$(this).parent().parent().remove();
		
		//looping tr
		$.each($('#list-detail >tr'), function(index, item){
			var indexLama = $(this).attr('id').match(/\d+/);
			var trBaru = $(this).attr('id').replace('_'+indexLama,'_'+index);
			$(this).attr('id',trBaru);		
					
			//looping td
			$.each($(this).find('.form-control'),function(key, val){
				var namaLama = $(this).attr('name');
				var namaBaru = namaLama.replace('['+indexLama+']','['+index+']');
				$(this).attr('name', namaBaru);
				
				var idLama = $(this).attr('id');
				var idBaru = idLama.replace('_'+indexLama+'_','_'+index+'_');
				$(this).attr('id', idBaru);
			});	
		});
		
	});
	
	
	
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		// ambil data trxKelas
	/* 	$('.text-danger').hide();
		$('#guruId').change(function(){
			var guruId = $('#guruId').val();
			$.ajax({
				url: 'trxAbsen/getByGuru.json',
				type: 'post',
				data: {guruId:guruId},
				dataType: 'json',
				success: function(hasil){
					if (hasil.data == 1) {
						$('.text-danger').show();
						//$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-danger').hide();		
					}
				}
			});
		}); */
		
		//=========== data get by id kelas ======================
			$('.text-danger').hide();
			$('#trxKelasId').change(function(){
				var id = $('#trxKelasId').val();
			$.ajax({
				url: 'trxKelas/getById.json',
				type: 'post',
				data: {id:id},
				dataType: 'json',
				success: function(hasil){
					if (hasil.data == null) {
						$('.text-danger').hide();
						//$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-danger').show();
						$('#siswaId').empty();
						$('#siswaId').append(
								'<option value="0">==Pilih siswa ==</option>');
						// looping data untuk menambahkan data ke list data
						$.each(hasil.data.detail, function(index, item) {
							$('#siswaId').append(
									'<option value="'+item.siswa.id+'">' + item.siswa.nmSiswa
											+ '</option>');
						});
					}
				}
			});
		});
		
		// ============== validate siswa change ======================
			$('.btn-add-detail').hide();
			$('#siswaId').hide();
			$('#trxKelasId').change(function(){
				var kelas = $('#trxKelasId').val();
				if (kelas == 0) {
					$('#siswaId').hide();
					$('.btn-add-detail').hide();
				} else {
					$('#siswaId').show();
				}
				
			});
			$('#siswaId').change(function(){
				var siswa = $('#siswaId').val();
				if (siswa == 0) {
					$('.btn-add-detail').hide();
				} else {
					$('.btn-add-detail').show();
				}
				
			});
			
			
		
		$("#trxKelasId").change(function() {
			var guru = $(this).find(':selected').attr('data-guru');
			var ta = $(this).find(':selected').attr('data-ta');
			$('#guruId').val(guru);
			$('#taId').val(ta);
		});
		$("#tglAbsen").datepicker({
			autoclose:true,
			format:'yyyy-mm-dd'
		});
		$('#data-detail-edit').DataTable();
		
		/* loadSiswa();
		 */loadTa();
		loadKelas();
		loadAbsen(); 
		loadData();
	});
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
