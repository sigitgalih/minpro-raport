<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Guru</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse" data-target="#data-input"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div id="data-input" class="collapse">
	<form action="" method="post" id="form-Guru"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			
			<div class="form-group">
				<label class="control-label col-md-2">NIK Guru</label>
				<div class="col-md-8">
					<input type="text" name="nik" id="nik" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">User Role</label>
				<div class="col-md-3">
					<select name="userId" id="userId" class="form-control">
						<option val="">--Masukkan Role--</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Nama Guru</label>
				<div class="col-md-8">
					<input type="text" name="nama" id="nama" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Username</label>
				<div class="col-md-8">
					<input type="text" name="username" id="username" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Password</label>
				<div class="col-md-8">
					<input type="password" name="password" id="password" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Jenis Kelamin</label>
				<div class="col-md-4">
					<select name="jk" id="jk" class="form-control">
						<option val="">--Masukkan Jenis Kelamin--</option>
						<option val="pria">Pria</option>
						<option val="wanita">Wanita</option>
					</select>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
     </div>
	
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Guru</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID Guru</th>
					<th>NIK Guru</th>
					<th>Nama Guru</th>
					<th>Username</th>
					<th>Jenis Kelamin</th>
					<th>User Role</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
function loadRole() {
	$.ajax({
		url : 'usersRole/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#userId').empty();
				$('#userId').append(
						'<option value="">Pilih Role</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#userId').append(
							'<option value="'+item.id+'">' + item.roleName
									+ '</option>');
				});
			}
		}
	});
}
	function loadData() {
		$.ajax({
			url : 'guru/load.json',
			type : 'get',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#list-data').empty();
					// looping data untuk menambahkan data ke list data
					$.each(hasil.data,function(index, item) {
						var items = '<tr>'
					 			  + '<td>'+ item.id +'</td>'
					 			  + '<td>'+ item.nik +'</td>'
								  + '<td>'+ item.nama+ '</td>'
								  + '<td>'+ item.username+ '</td>'
								  + '<td>'+ item.jk+ '</td>'
								  + '<td>'+ item.userId+ '</td>'
								  + '<td>'
								       + '<button type="button" data-toggle="collapse" data-target="#data-input" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
						     + '&nbsp&nbsp<button type="button" data-toggle="collapse" data-target="#data-input" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
								  +'</td></tr>';	
							
						// menambahkan item ke list data	                
						$('#list-data').append(items);
					});		  					
			    }
			}
		});
	}

	$("#form-Guru").submit(function() {
		$.ajax({
			url : 'guru/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-Guru").trigger('reset');
					$('#proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'guru/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#nik').val(hasil.data.nik);
					$('#nama').val(hasil.data.nama);
					$('#username').val(hasil.data.username);
					$('#userId').val(hasil.data.userId);
					$('#jk').val(hasil.data.jk);
					$('#proses').val('update');
				}
			}
		});
	});
		

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'guru/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#nik').val(hasil.data.nik);
					$('#nama').val(hasil.data.nama);
					$('#password').val(hasil.data.password);
					$('#jk').val(hasil.data.jk);
					$('#userId').val(hasil.data.userId);
					$('#proses').val('delete');
				}
			}
		});
	});
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		loadRole();
		loadData();
	});
</script>
