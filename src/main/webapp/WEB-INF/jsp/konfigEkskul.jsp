<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Konfigurasi Ekstrakurikuler</h3>
		<div class="pull-right">
		<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse"
				data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button></div>
	</div>
	<div id="data-input" class="collapse">
	<form action="" method="post" id="form-konfig"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="" value="1" class="form-control id">
			<input type="hidden" name="proses" id="" value="insert" class="form-control proses">
			
			<div class="form-group">
				<label class="control-label col-md-2">Tanggal Ekskul :</label>
				<div class="col-md-4 input-group">
				<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
					<input type="text" name="tglEkskul" id="tglEkskul" class="form-control tglEkskul">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Nama Ekstrakurikuler :</label>
				<div class="col-md-4">
					<select name="ekskulId" id="" class="form-control ekskulId">
						<option value="">== Pilih Ekstrakurikuler ==</option>
					</select>	
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Guru Pembimbing :</label>
				<div class="col-md-4">
				<select name="guruId" id="" class="form-control guruId">
						<option value="">== Pilih Guru ==</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Semester :</label>
				<div class="col-md-4">
				<select name="semesterId" id="" class="form-control semesterId">
						<option value="">== Pilih Semester ==</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Tahun Ajaran :</label>
				<div class="col-md-4">
				<select name="taId" id="" class="form-control taId">
						<option value="">== Pilih Tahun Ajaran ==</option>
				</select>
				</div>
			</div>
			
			<fieldset>
			<legend>Detail Kelas</legend>
			</fieldset>
			<!-- Form Detail Kelas -->
			<div class="form-group">
				<input type="hidden" name="trxKelas" id="" value="1" class="form-control trxKelas">
				<label class="control-label col-md-2">Siswa : </label>
					<div class="col-md-2">
						<select name="siswaId" id="siswaId" class="form-control">
							<option value="">== Pilih Siswa ==</option>
						</select>
					</div>	
					<label class="control-label col-md-1">Nilai Harian : </label>
					<div class="col-md-2">
						<input name="nilai" id="nilai" class="form-control" />
					</div>
				<div class="col-md-1">
					<button type="button" id="btn-add-detail"  class="btn btn-success"><i class="fa fa-plus"></i></button>
				</div>
			</div>
			<!-- Tabel detail siswa per kelas -->
		<div class="form-group">
				<table class="table table-responsive">
					<thead>
						<tr>
							<td>
								<th class="col-md-6">Nama Siswa</th>
							</td>
							<td>
								<th class="col-md-6">Nilai Harian</th>
							</td>
						</tr>
					</thead>
					
					<tbody id="list-detail">
						
					</tbody>
				</table>
		</div>
		</div>
		
		
			
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
     </div>
	
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Data Ekstrakurikuler</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tgl Ekskul</th>
					<th>Ekskul</th>
					<th>Guru Pembimbing</th>
					<th>Semester</th>
					<th>Tahun Ajaran</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
//=========================load semester data ======================
function loadSemester() {
	$.ajax({
		url : 'semester/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('.semesterId').empty();
				$('.semesterId').append('<option value="">== Pilih Semester ==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('.semesterId').append('<option value="'+item.id+'">' + item.nmSemester+ '</option>');
				});
			}
		}
	});
}

// =========================load kelas data ======================
function loadEkskul() {
	$.ajax({
		url : 'ekskul/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('.ekskulId').empty();
				$('.ekskulId').append(
						'<option value="">Pilih Ekskul</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('.ekskulId').append(
							'<option value="'+item.id+'">' + item.nmEkskul
									+ '</option>');
				});
			}
		}
	});
}
//=========================load Guru data ======================
function loadGuru() {
	$.ajax({
		url : 'guru/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('.guruId').empty();
				$('.guruId').append(
						'<option value="">Pilih Guru</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('.guruId').append(
							'<option value="'+item.id+'">' + item.nama
									+ '</option>');
				});
			}
		}
	});
}
//=========================load TAHUN AJARAN data ======================
function loadTa() {
	$.ajax({
		url : 'ta/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('.taId').empty();
				$('.taId').append(
						'<option value="">Pilih Tahun Ajaran</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('.taId').append(
							'<option value="'+item.id+'">' + item.tahunAjaran
									+ '</option>');
				});
			}
		}
	});
}

//===============LOAD SISWA==================
function loadSiswa() {
		$.ajax({
			url : 'siswa/load.json',
			type : 'GET',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#siswaId').empty();
					$("#siswaId").append('<option value="">== Pilih Siswa ==</option>');
					$.each(hasil.data, function(index, item){
						$('#siswaId').append('<option value="'+item.id+'">'+item.nmSiswa+'</option>');
					});
			    }
			}
		});
	}

	function loadData() {
		$.ajax({
			url : 'konfigEkskul/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#list-data').html(data);
			}
		});
	}

	$("#form-konfig").submit(function() {
		$.ajax({
			url : 'konfigEkskul/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-konfig").trigger('reset');
					$('.proses').val('insert');
					$('#list-detail').empty();
					loadData();
				}
			}
		});
		return false;
	});

	
	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'konfigEkskul/getById.json',
			type : 'post',
			data : {id : id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.ekskulId').val(hasil.data.ekskulId);
					$('.tglEkskul').datepicker('update', hasil.data.tglEkskul);
					$('.guruId').val(hasil.data.guruId);
					$('.semesterId').val(hasil.data.semesterId);
					$('.taId').val(hasil.data.taId);
					$('.proses').val('update');
					$("#list-detail").empty();	
					// load data detail
					$.each(hasil.data.konfigEkskulDetail, function(index, item){
						var dat1 = '<tr id="konfigEkskulDetail_'+index+'">' +
						'<td><input type="hidden" name="konfigEkskulDetail['+index+'].siswaId" id="konfigEkskulDetail_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+  
						'<td class="subtot"><input type="hidden" name="konfigEkskulDetail['+index+'].nilai" id="konfigEkskulDetail_'+index+'nilai" value="'+item.nilai+'" class="form-control"/>' + item.nilai + '</td>'+ 
						'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';
						$("#list-detail").append(dat1);	
					});
				}
			}
		});
	});

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'konfigEkskul/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.ekskulId').val(hasil.data.ekskulId);
					$('.tglEkskul').val(hasil.data.tglEkskul);
					$('.semesterId').val(hasil.data.semesterId);
					$('.guruId').val(hasil.data.guruId);
					$('.taId').val(hasil.data.taId);
					$('.proses').val('delete');
					$.each(hasil.data.konfigEkskulDetail, function(index, item){
						var dat1 = '<tr id="konfigEkskulDetail_'+index+'">' +
						'<td><input type="hidden" name="konfigEkskulDetail['+index+'].siswaId" id="konfigEkskulDetail_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+  
						'<td class="subtot"><input type="hidden" name="konfigEkskulDetail['+index+'].nilai" id="konfigEkskulDetail_'+index+'nilai" value="'+item.nilai+'" class="form-control"/>' + item.nilai + '</td>'+ 
						'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-trash-o"></i></button></td></tr>';
						});
				}
			}
		});
	});
	
	
	$('#btn-add-detail').click(function() {
		var siswaId=$('#siswaId').val();
		var nmSiswa = $("#siswaId :selected").text();
		var nilai=$('#nilai').val();
		var index=$('#list-detail > tr').length;
		var data='<tr id="konfigEkskulDetail'+index+'">'+
		  '<td><input type="hidden" name="konfigEkskulDetail['+index+'].siswaId" id="konfigEkskulDetail['+index+'].siswaId" value="'+siswaId+'" class="form-control" />'+nmSiswa+'</td>'+
		  '<td class="subtot"><input type="hidden" name="konfigEkskulDetail['+index+'].nilai" id="konfigEkskulDetail['+index+'].nilai" value="'+nilai+'" class="form-control" />'+nilai+'</td>'+
		  '<td><button type="button" class="btn btn-danger btn-delete-detail" value="'+index+'" ><i class="fa fa-remove"></i></button></td>'
		  '</tr>';
	  $('#list-detail').append(data);
	  
	  $('#siswaId').val('');
	  $('#nilai').val('');
	});
	
	$("#list-detail").on("click", ".btn-delete-detail", function() {
		$(this).parent().parent().remove();
		
		//looping tr
		$.each($('#list-detail >tr'), function(index, item){
			var indexLama = $(this).attr('id').match(/\d+/);
			var trBaru = $(this).attr('id').replace('_'+indexLama,'_'+index);
			$(this).attr('id',trBaru);		
					
			//looping td
			$.each($(this).find('.form-control'),function(key, val){
				var namaLama = $(this).attr('name');
				var namaBaru = namaLama.replace('['+indexLama+']','['+index+']');
				$(this).attr('name', namaBaru);
				
				var idLama = $(this).attr('id');
				var idBaru = idLama.replace('_'+indexLama+'_','_'+index+'_');
				$(this).attr('id', idBaru);
			});	
		});
		
	});

	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
			$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		$("#tglEkskul").datepicker({
			autoclose:true,
			format:'yyyy-mm-dd'
		});
		
		loadEkskul();
		loadSemester();
		loadGuru();
		loadTa();
		loadSiswa()
		loadData();
	});
</script>
