<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">Form Kelas</h3>
	</div>
	<form action="" method="post" id="form-statusAbsen"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			
			<div class="form-group">
				<label class="control-label col-md-2">Nama Status</label>
				<div class="col-md-3">
						<select name="keterangan" id="keterangan" class="form-control keterangan"
						required oninvalid="this.setCustomValidity('Harus Diplih..!')" 
						onchange="this.setCustomValidity('')"> 
							<option value="">==Pilih Keterangan==</option>
							<option value="alfa">Alfa</option>
							<option value="ijin">Ijin</option>
							<option value="sakit">Sakit</option>
							<option value="hadir">Hadir</option>
						</select>
							<span><b class="text-danger">Jenis Absensi Sudah dipake ..!</b></span>
				
					</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
</div>

<div class="box box-danger">
	<div class="box-header">
		<h3 class="box-title">List Status Absen</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive" id="data">
			<thead>
				<tr>
					<th>ID Kelas</th>
					<th>Nama Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
	function loadData() {
		$.ajax({
			url : 'statusAbsen/load.json',
			type : 'get',
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == 'success') {
					// sebelum menambah data di kosongkan dulu
					$('#list-data').empty();
					// looping data untuk menambahkan data ke list data
					$.each(hasil.data,function(index, item) {
						var items = '<tr>'
					 			  + '<td>'+ item.id +'</td>'
					 			  + '<td>'+ item.keterangan +'</td>'
								  + '<td>'
								       + '<button type="button" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
						     + '&nbsp&nbsp<button type="button" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
								  +'</td></tr>';	
							
						// menambahkan item ke list data	                
						$('#list-data').append(items);
					});		  					
			    }
			}
		});
	}

	$("#form-statusAbsen").submit(function() {
		$.ajax({
			url : 'statusAbsen/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-statusAbsen").trigger('reset');
					$('#proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	
	
	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'statusAbsen/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#keterangan').val(hasil.data.keterangan);
					$('#proses').val('update');
				}
			}
		});
	});
	

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'statusAbsen/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#keterangan').val(hasil.data.keterangan);
					$('#proses').val('delete');
				}
			}
		});
	});
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		
		// panggil method loadData
		// ======== Status Absen =======
			$(".text-danger").hide();
			$('#keterangan').change(function(){
			var keterangan = $('#keterangan').val();
			$.ajax({
				url: 'statusAbsen/getByNamaStatus.json',
				type: 'post',
				data: {keterangan:keterangan},
				dataType: 'json',
				success: function(hasil){
					if (hasil.data == 1) {
						$('.text-danger').show();
						$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-danger').hide();		
					}
				}
			});
		});
			
		
		//====
		
		loadData();
	});
</script>
