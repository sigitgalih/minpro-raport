<style>
.modal-header {
    padding:9px 15px;
    color:white;
    border-bottom:1px solid #eee;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }
</style>
<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Guru</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse"
				data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button>
		</div>
	</div>
	<div id="data-input" class="collapse">
		<form action="" method="post" id="" class="form-horizontal form-Guru">

			<div class="box-body">
				<input type="hidden" name="id" id="" value="1"
					class="form-control id"> <input type="hidden" name="proses"
					id="" value="insert" class="form-control proses">

				<div class="form-group">
					<label class="control-label col-md-2">NIK Guru</label>
					<div class="col-md-8">
						<input type="text" required  name="nik" id="" class="form-control nik"
						oninvalid="this.setCustomValidity('Harus Diisi Angka')" 
						onchange="this.setCustomValidity('')" pattern="^[0-9]*" maxlength="16">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Nama Guru</label>
					<div class="col-md-8">
						<input type="text" required name="nama" id="" class="form-control nama"
						onInvalid="this.setCustomValidity('Nama guru harus diisi')"
						onchange="this.setCustomValidity('')">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Jenis Kelamin</label>
					<div class="col-md-4">
						<select required name="jk" id="" class="form-control jk"
						onInvalid="this.setCustomValidity('Jenis kelamin harus dipilih')"
						onchange="this.setCustomValidity('')">
							<option val="">--Masukkan Jenis Kelamin--</option>
							<option val="pria">Pria</option>
							<option val="wanita">Wanita</option>
						</select>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
			</div>
		</form>
	</div>
	<!-- Modal =====================EDIT========================-->
	<div class="modal fade" id="modalEdit" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Guru</h4>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-GuruEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">

							<div class="form-group">
								<label class="control-label col-md-2">NIK Guru</label>
								<div class="col-md-8">
									<input type="text" required name="nik" id="" class="form-control nik" readonly
									onInvalid="this.setCustomValidity('NIK harus diisi angka')"
									onchange="this.setCustomValidity('')" pattern="^[0-9]*" maxlength="16">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Nama Guru</label>
								<div class="col-md-8">
									<input type="text" required name="nama" id="" class="form-control nama"
									onInvalid="this.setCustomValidity('Nama guru harus diisi')"
									onchange="this.setCustomValidity('')">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Jenis Kelamin</label>
								<div class="col-md-4">
									<select required name="jk" id="" class="form-control jk"
									onInvalid="this.setCustomValidity('Jenis kelamin harus dipilih')"
									onchange="this.setCustomValidity('')">
										<option val="">--Masukkan Jenis Kelamin--</option>
										<option val="pria">Pria</option>
										<option val="wanita">Wanita</option>
									</select>
								</div>
							</div>
						</div>
						<div class="modal-footer">
					<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
					<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
		<!-- Modal =====================HAPUSSS========================-->
	<div class="modal fade" id="modalHapus" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="alert alert-danger has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Peringatan..!!</h4></center>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-GuruHapus">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">

							<div class="form-group">
								<label class="control-label col-md-2">NIK Guru</label>
								<div class="col-md-8">
									<input type="text" name="nik" id="" class="form-control nik" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Nama Guru</label>
								<div class="col-md-8">
									<input type="text" name="nama" id="" class="form-control nama" readonly>
								</div>
							</div>
							<hr>
							<h5>Apakah Anda Yakin Ingin Menghapus Data Diatas</h5>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-danger col-md-2">Hapus</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Guru</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive" id="data-table">
			<thead>
				<tr>
					<th>ID Guru</th>
					<th>NIK Guru</th>
					<th>Nama Guru</th>
					<th>Jenis Kelamin</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>
	function loadRole() {
		$
				.ajax({
					url : 'usersRole/load.json',
					type : 'GET',
					dataType : 'json',
					success : function(hasil) {
						if (hasil.result == 'success') {
							// sebelum menambah data di kosongkan dulu
							$('.userId').empty();
							$('.userId').append(
									'<option value="">Pilih Role</option>');
							// looping data untuk menambahkan data ke list data
							$.each(hasil.data, function(index, item) {
								$('.userId').append(
										'<option value="'+item.id+'">'
												+ item.roleName + '</option>');
							});
						}
					}
				});
	}
	
	function loadData() {
		$.ajax({
			url : 'guru/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#list-data').html(data);
			}
		});
	}
	
	// ===================== SUBMIT SIMPAN ================
	$(".form-Guru").submit(function() {
		$.ajax({
			url : 'guru/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-Guru").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT EDIT ================
	$(".form-GuruEdit").submit(function() {
		$.ajax({
			url : 'guru/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-GuruEdit").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT HAPUS ================
	$(".form-GuruHapus").submit(function() {
		$.ajax({
			url : 'guru/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				loadData();
				if (hasil.result == "success") {
					$(".form-GuruHapus").trigger('reset');
					$('.proses').val('insert');
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'guru/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.nik').val(hasil.data.nik);
					$('.nama').val(hasil.data.nama);
					$('.jk').val(hasil.data.jk);
					$('.proses').val('update');
					$(".form-Guru").trigger('reset');
				}
			}
		});
	});

	
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'guru/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.nik').val(hasil.data.nik);
					$('.nama').val(hasil.data.nama);
					$('.jk').val(hasil.data.jk);
					$('.proses').val('delete');
					$(".form-Guru").trigger('reset');
				}
			}
		});
	});
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		loadRole();
		loadData();
	});
</script>
