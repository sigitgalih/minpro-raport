<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Nilai Harian</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse" data-target="#data-input"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div id="data-input" class="collapse">
	<form action="" method="post" id="form-harian"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			
			<div class="form-group">
			<label class="control-label col-md-2">Input Tanggal : </label> 
				<div class="col-md-2 input-group">
				<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
					<input type="text" name="tglNilai" id="tglNilai" class="form-control"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Pilih Semester : </label>
				<div class="col-md-4">
				<select name="semesterId" id="semesterId" class="form-control">
						<option value="">== Pilih Semester ==</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Pilih Mata Pelajaran : </label>
				<div class="col-md-4">
				<select name="mapelId" id="mapelId" class="form-control">
						<option value="">== Pilih Mapel==</option>
				</select>
				</div>
			</div>
			
		 	<!--================DETAILNYA================== -->
		 	<fieldset>
				<h3>Detail Nilai Harian</h3>
			</fieldset>
			<!-- Form Detail Kelas -->
			<div class="form-group">
				<div class="form-group">
				<label class="control-label col-md-2">Pilih Kelas : </label>
				<div class="col-md-2">
					<select name="trxKelasId" id="trxKelasId" class="form-control">
						<option value="">== Piih Kelas ==</option>
					</select>
					<!--  <b class="text-warning" >Ketemu!</b> -->
				</div>
				
				<div class="col-md-3">
					<input type="text" name="guruId" id="guruId" class="form-control" readonly ></input>
				</div>
				<div class="col-md-3">
					<input type="text" name="taId" id="taId" class="form-control" readonly ></input>
				</div>
			</div>
			</div>
			
			<div class="form-group">
				<div class="form-group">
					<label class="control-label col-md-2">Input Siswa : </label>
					<div class="col-md-2">
					<select name="siswaId" id="siswaId" class="form-control">
						<option value="">== Pilih Siswa ==</option>
					</select>
					</div>	
				<div class="form-group">
					<div class="col-md-2">
						<input name="nilai" id="nilai" class="form-control" placeholder="Masukakn Nilai Siswa"/>	
					</div>
					<div class="col-md-1">
						<button type="button" id="btn-add-detail"  class="btn btn-success"><i class="fa fa-plus"></i></button>
					</div>
				</div>
				
			</div>
				
			</div>
			
			<!-- Tabel detail siswa per kelas -->
			<div class="form-group">
					<table class="table table-responsive">
						<thead>
							<tr>
								<th class="">Kelas</th>
								<th class="">Siswa</th>
								<th class="">Nilai</th>
							</tr>
						</thead>
						<tbody id="list-detail">
						</tbody>
					</table>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">TOTAL</label>
				<div class="col-md-8">
					<input type="text" name="totNilai" id="totNilai" class="form-control" value="0">
				</div>
			</div>
		</div>

			
		<div class="box-footer">
			<button type="submit" id="tombol" name="tombol" class="btn btn-success col-md-2">Simpan</button>
		</div>

	</form>
     </div>
	
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Data Nilai Harian</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive" id="data-nilai">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tanggal Ulangan</th>
					<th>Semester</th>
					<th>Mata Pelajaran</th>
					<th>Total Nilai</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
			
		</table>
	</div>
</div>

<script>
// =========================load KELAS ======================
function loadKelas() {
	$.ajax({
		url : 'trxKelas/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#trxKelasId').empty();
				$('#trxKelasId').append(
						'<option value="">==Pilih Kelas==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#trxKelasId').append(
							'<option value="'+item.id+'" data-guru="'+item.guru.nama+'" data-ta="'+item.ta.tahunAjaran+'">' +item.kelas.nama+ '</option>');
				});
				
			}
		}
	});
}
	
//=========================load Semester data ======================
function loadSemester() {
	$.ajax({
		url : 'semester/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#semesterId').empty();
				$('#semesterId').append(
						'<option value="">==Pilih Semester==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#semesterId').append(
							'<option value="'+item.id+'">' + item.nmSemester
									+ '</option>');
				});
			}
		}
	});
}
//=========================load MATA PELAJARAN data ======================
function loadMapel() {
	$.ajax({
		url : 'mapel/load.json',
		type : 'GET',
		dataType : 'json',
		success : function(hasil) {
			if (hasil.result == 'success') {
				// sebelum menambah data di kosongkan dulu
				$('#mapelId').empty();
				$('#mapelId').append(
						'<option value="">==Pilih Mata Pelajaran==</option>');
				// looping data untuk menambahkan data ke list data
				$.each(hasil.data, function(index, item) {
					$('#mapelId').append(
							'<option value="'+item.id+'">' + item.nmMapel
									+ '</option>');
				});
			}
		}
	});
}
	function loadData() {
		$.ajax({
			url : 'trxNilaiHarian/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#list-data').html(data);
			}
		});
	}

	$("#form-harian").submit(function() {
		$.ajax({
			url : 'trxNilaiHarian/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$("#form-harian").trigger('reset');
					$('#proses').val('insert');
					$('#list-detail').empty();
					loadData();
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'trxNilaiHarian/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#tglNilai').val(hasil.data.tglNilai);
					$('#semesterId').val(hasil.data.semesterId);
					$('#mapelId').val(hasil.data.mapelId);
					$('#totNilai').val(hasil.data.totNilai);
					$('#proses').val('update');
					$("#list-detail").empty();	
					// load data detail
					$.each(hasil.data.detailNilai, function(index, item){
						var dat1 = '<tr id="detailNilai_'+index+'">' +
						'<td><input type="hidden" name="detailNilai['+index+'].trxKelasId" id="detailNilai_'+index+'_trxKelasId" value="'+item.trxKelasId+'" class="form-control"/>' + item.kelas.kelas.nama + '</td>'+  
						'<td><input type="hidden" name="detailNilai['+index+'].siswaId" id="detailNilai_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+  
						'<td class="subtot"><input type="hidden" name="detailNilai['+index+'].nilai" id="detailNilai_'+index+'nilai" value="'+item.nilai+'" class="form-control"/>' + item.nilai + '</td>'+ 
						'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-remove-o"></i></button></td></tr>';			
				
						$("#list-detail").append(dat1);	
					});
				}
			}
		});
	});
		

    
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'trxNilaiHarian/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#id').val(hasil.data.id);
					$('#tglNilai').val(hasil.data.tglNilai);
					$('#semesterId').val(hasil.data.semesterId);
					$('#mapelId').val(hasil.data.mapelId);
					$('#totNilai').val(hasil.data.totNilai);
					$('#proses').val('delete');
					$("#list-detail").empty();	
					// load data detail
					$.each(hasil.data.detailNilai, function(index, item){
						var dat1 = '<tr id="detailNilai_'+index+'">' +
						'<td><input type="hidden" name="detailNilai['+index+'].trxKelasId" id="detailNilai_'+index+'_trxKelasId" value="'+item.trxKelasId+'" class="form-control"/>' + item.kelas.kelas.nama + '</td>'+  
						'<td><input type="hidden" name="detailNilai['+index+'].siswaId" id="detailNilai_'+index+'_siswaId" value="'+item.siswaId+'" class="form-control"/>' + item.siswa.nmSiswa + '</td>'+  
						'<td class="subtot"><input type="hidden" name="detailNilai['+index+'].nilai" id="detailNilai_'+index+'nilai" value="'+item.nilai+'" class="form-control"/>' + item.nilai + '</td>'+ 
						'<td><button type="button" class="btn btn-danger btn-delete-detail"><i class="fa fa-remove-o"></i></button></td></tr>';			
				
						$("#list-detail").append(dat1);	
					});
				}
			}
		});
	});
	
	$('#btn-add-detail').click(function() {
		var siswaId=$('#siswaId').val();
		var trxKelasId=$('#trxKelasId').val();
		var nmSiswa = $("#siswaId :selected").text();
		var kelas = $("#trxKelasId :selected").text();
		var nilai=$('#nilai').val();
		var index=$('#list-detail > tr').length;
		var data='<tr id="detailNilai'+index+'">'+
		  '<td><input type="hidden" name="detailNilai['+index+'].trxKelasId" id="detailNilai['+index+'].trxKelasId" value="'+trxKelasId+'" class="form-control" />'+kelas+'</td>'+
		  '<td><input type="hidden" name="detailNilai['+index+'].siswaId" id="detailNilai['+index+'].siswaId" value="'+siswaId+'" class="form-control" />'+nmSiswa+'</td>'+
		  '<td class="subtot"><input type="hidden" name="detailNilai['+index+'].nilai" id="detailNilai['+index+'].nilai" value="'+nilai+'" class="form-control" />'+nilai+'</td>'+
		  '<td><button type="button" class="btn btn-danger btn-delete-detail" value="'+index+'" ><i class="fa fa-remove"></i></button></td>'
		  '</tr>';
	  $('#list-detail').append(data);
	  var rata = $("#totNilai").val();
	  rata = parseInt(rata) + parseInt(nilai);
	  $("#totNilai").val(rata);
	  
	  $('#siswaId').val("");
	  $('#nilai').val("");
	});
	
	$("#list-detail").on("click", ".btn-delete-detail", function() {

		var total = $("#totNilai").val(); 
		var sub = $(this).closest('tr').find(".subtot").text(); 
		var hasil = parseInt(total)-parseInt(sub);
		$("#totNilai").val(hasil);
		$(this).parent().parent().remove();
		
		//looping tr
		$.each($('#list-detail >tr'), function(index, item){
			var indexLama = $(this).attr('id').match(/\d+/);
			var trBaru = $(this).attr('id').replace('_'+indexLama,'_'+index);
			$(this).attr('id',trBaru);		
					
			//looping td
			$.each($(this).find('.form-control'),function(key, val){
				var namaLama = $(this).attr('name');
				var namaBaru = namaLama.replace('['+indexLama+']','['+index+']');
				$(this).attr('name', namaBaru);
				
				var idLama = $(this).attr('id');
				var idBaru = idLama.replace('_'+indexLama+'_','_'+index+'_');
				$(this).attr('id', idBaru);
			});	
		});
		
	});
	
	
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		$(".text-warning").hide();
		// ambil data trxKelas 
		$("#trxKelasId").change(function(){
			var guru = $(this).find(':selected').attr('data-guru');
			var ta = $(this).find(':selected').attr('data-ta');
			$('#guruId').val(guru);
			$('#taId').val(ta);
		});
		
		$("#trxKelasId").change(function() {
			var id = $('#trxKelasId').val();
			$.ajax({
				url : 'trxKelas/getById.json',
				type: 'post',
				data: {id:id},
				dataType: 'json',
				success: function(hasil){
					if(hasil.data == null){
						$('.text-warning').hide();
						//$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-warning').show();
						//$(':input[type="submit"]').prop('disabled', false);
						$('#siswaId').empty();
						$('#siswaId').append(
								'<option value="">==Pilih Siswas==</option>');
						// looping data untuk menambahkan data ke list data
						$.each(hasil.data.detail, function(index, item) {
							$('#siswaId').append(
									'<option value="'+item.id+'" >' +item.siswa.nmSiswa+ '</option>');
						});
						
					}
				}
			});

		});
		$("#siswaId").change(function() {
			$("#nilai").focus();
		});
		/* $("#tglNilai").change(function() {
			$("#nilai").focus();
		}); */
		$("#tglNilai").datepicker({
			autoclose:true,
			format:'yyyy-mm-dd'
		});
		//loadSiswa();
		loadKelas();
		loadSemester();
		//loadTa();
		//loadGuru();
		loadMapel();
		loadData();
	});
</script>
