<style>
.modal-header {
    padding:9px 15px;
    color:white;
    border-bottom:1px solid #eee;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }
</style>
<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Form Kelas</h3>
		<div class="pull-right">
			<button class="btn btn-success" data-toggle="collapse"
				data-target="#data-input" id="plus">
				<i class="fa fa-plus-circle"></i>
			</button>
			<button class="btn btn-danger" data-toggle="collapse" data-target="#data-input" id="min" style="display:none;">
				<i class="fa fa-minus-circle"></i>
			</button>
		</div>
	</div>
	<div id="data-input" class="collapse">
		<form action="" method="post" id="" class="form-horizontal form-kelas">

			<div class="box-body">
				<input type="hidden" name="id" id="" value="1"
					class="form-control id"> <input type="hidden" name="proses"
					id="" value="insert" class="form-control proses">
				<div class="form-group">
					<label class="control-label col-md-2"> Kelas</label>
					<div class="col-md-8">
						<input type="text" required name="nama" id="" class="form-control nama"
						onInvalid="this.setCustomValidity('Nama kelas harus diisi')"
						onchange="this.setCustomValidity('')">
						<span><b class="text-warning" style="color:red;">Kelas Sudah ada..!</b></span>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
			</div>
		</form>
	</div>
	<!-- Modal =====================EDIT========================-->
	<div class="modal fade" id="modalEdit" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Kelas</h4>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-kelasEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
						<div class="form-group">
								<label class="control-label col-md-2"> Kelas</label>
								<div class="col-md-8">
									<input type="text" name="nama" id="" class="form-control nama">
								</div>
							</div>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-success col-md-2">Simpan</button>
					<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
		<!-- Modal =====================HAPUSSS========================-->
	<div class="modal fade" id="modalHapus" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="alert alert-danger has-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Peringatan..!!</h4></center>
				</div>
				<div class="modal-body">
					<form action="" method="post" id=""
						class="form-horizontal form-kelasEdit">

						<div class="box-body">
							<input type="hidden" name="id" id="" value="1"
								class="form-control id"> <input type="hidden" name="proses"
								id="" value="insert" class="form-control proses">
							<div class="form-group">
								<label class="control-label col-md-2"> Kelas</label>
								<div class="col-md-8">
									<input type="text" name="nama" id="" class="form-control nama" readonly>
								</div>
							</div>
							<hr>
							<h5>Apakah Anda Yakin Ingin Menghapus Data Diatas</h5>
						</div>
						<div class="modal-footer">
				<button type="submit" id="tombol" name="tombol"
					class="btn btn-danger col-md-2">Hapus</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Kembali</button>
				</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Kelas</h3>
	</div>
	<div class="box-body">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ID Kelas</th>
					<th> Kelas</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<script>

	function loadData() {
		$.ajax({

			url : 'kelas/load.json',
					type : 'get',
					dataType : 'json',
					success : function(hasil) {
						if (hasil.result == 'success') {
							// sebelum menambah data di kosongkan dulu
							$('#list-data').empty();
							// looping data untuk menambahkan data ke list data

							$.each(hasil.data,function(index, item) {
								var items = '<tr>'
								+ '<td>'
								+ item.id
								+ '</td>'
								+ '<td>'
								+ item.nama
								+ '</td>'
								+ '<td>'
								+ '<button type="button" data-toggle="modal" data-target="#modalEdit" class="btn btn-warning btn-edit" value="'+item.id+'" ><i class="fa fa-edit"></i></button>'
								+ '&nbsp&nbsp<button type="button" data-toggle="modal" data-target="#modalHapus" class="btn btn-danger btn-delete" value="'+item.id+'"><i class="fa fa-trash"></i></button>'
								+ '</td></tr>';
								// menambahkan item ke list data	               
								$('#list-data').append(items);
							});
						}
					}
				});

		}
	// ===================== SUBMIT SIMPAN ================
	$(".form-kelas").submit(function() {
		$.ajax({
			url : 'kelas/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-kelas").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT EDIT ================
	$(".form-kelasEdit").submit(function() {
		$.ajax({
			url : 'kelas/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				if (hasil.result == "success") {
					$(".form-kelasEdit").trigger('reset');
					$('.proses').val('insert');
					loadData();
				}
			}
		});
		return false;
	});
	// ===================== SUBMIT HAPUS ================
	$(".form-kelasHapus").submit(function() {
		$.ajax({
			url : 'kelas/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(hasil) {
				alert(hasil.result);
				loadData();
				if (hasil.result == "success") {
					$(".form-kelasHapus").trigger('reset');
					$('.proses').val('insert');
				}
			}
		});
		return false;
	});

	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'kelas/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.nama').val(hasil.data.nama);
					$('.proses').val('update');
					$(".form-kelas").trigger('reset');
				}
			}
		});
	});
	
	$("#list-data").on("click", ".btn-delete", function() {
		var id = $(this).val();
		$.ajax({
			url : 'kelas/getById.json',
			type : 'post',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('.id').val(hasil.data.id);
					$('.nama').val(hasil.data.nama);
					$('.proses').val('delete');
					$(".form-kelas").trigger('reset');
				}
			}
		});
	});
	
	// ================validation form===============
		
		$('.nama').keyup(function(){
			var nama = $('.nama').val();
			$.ajax({
				url : 'kelas/getByNama.json',
				type: 'post',
				data: {nama:nama},
				dataType: 'json',
				success: function(hasil){
					if(hasil.data == 1){
						$('.text-warning').show();
						$(':input[type="submit"]').prop('disabled', true);
					}else{
						$('.text-warning').hide();
						$(':input[type="submit"]').prop('disabled', false);
					}
				}
			});
		});

	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		$("#plus").click(function(){
			$("#plus").hide();
			$("#min").show();
		});
		$("#min").click(function(){
			$("#min").hide();
			$("#plus").show();
		});
		loadData();
		$('.text-warning').hide();
	});
</script>
