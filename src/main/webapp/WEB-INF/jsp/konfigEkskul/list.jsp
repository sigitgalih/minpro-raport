<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${data}">
	<tr>
		<td>${item.id}</td>
		<td>${item.tglEkskul}</td>
		<td>${item.ekskul.nmEkskul}</td>
		<td>${item.guru.nama}</td>
		<td>${item.semester.nmSemester}</td>
		<td>${item.ta.tahunAjaran}</td>
		
		<td>
			<button type="button" class="btn btn-success btn-xs btn-edit" value="${item.id }"><i class="fa fa-edit"></i></button>
			<button type="button" class="btn btn-danger btn-xs btn-delete" value="${item.id }"><i class="fa fa-trash"></i></button>
		</td>
	</tr>
</c:forEach>