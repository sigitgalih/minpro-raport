package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.EkskulModel;

public interface EkskulDao {
	public List<EkskulModel> get() throws Exception;
	public void insert(EkskulModel model) throws Exception;
	public EkskulModel getById(int id) throws Exception;
	public int getByNamaEkskul(String nmEkskul) throws Exception;
	public void update(EkskulModel model) throws Exception;
	public void delete(EkskulModel model) throws Exception;
	public void save(EkskulModel model) throws Exception;
}
