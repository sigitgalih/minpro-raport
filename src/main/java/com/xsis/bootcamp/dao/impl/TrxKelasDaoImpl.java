package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.TrxKelasDao;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.model.TrxKelasModel;

@Repository
public class TrxKelasDaoImpl implements TrxKelasDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TrxKelasModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<TrxKelasModel> result = session.createQuery("select sis from TrxKelasModel as sis").list();
		return result;
	}

	@Override
	public void insert(TrxKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public TrxKelasModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		TrxKelasModel result = session.get(TrxKelasModel.class, id);
        return result;
	}

	@Override
	public void update(TrxKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
		String hql = "delete from TrxKelasDetailModel where trxKelasId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		if(model.getTrxKelasDetail().size()>0){
			for (TrxKelasDetailModel item : model.getTrxKelasDetail()){
				item.setTrxKelasId(model.getId());
				
				session.save(item);
			}
		}
	}

	@Override
	public void delete(TrxKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "delete from TrxKelasDetailModel where trxKelasId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		session.delete(model);

	}
	
	@Override
	public void save(TrxKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<TrxKelasDetailModel> items = model.getTrxKelasDetail();
		if(items.size()>0){
			for (TrxKelasDetailModel item : items) {
				item.setTrxKelasId(model.getId());
				session.save(item);
			}
		}

	}

	/*@Override
	public List<TrxKelasModel> getByIdKelas() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<TrxKelasModel> result = session.createQuery("select kelas from TrxKelasModel as kelas").list();
		return result;
	}*/




	

}
