package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.KonfigEkskulDetailDao;
import com.xsis.bootcamp.model.KonfigEkskulDetailModel;

@Repository
public class KonfigEkskulDetailDaoImpl implements KonfigEkskulDetailDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<KonfigEkskulDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KonfigEkskulDetailModel> result = session.createQuery("select trx from KonfigEkskulDetailModel as trx").list();
		return result;
	}

	@Override
	public void insert(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public KonfigEkskulDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		KonfigEkskulDetailModel result = session.get(KonfigEkskulDetailModel.class, id);
        return result;
	}

	@Override
	public void update(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

}
