package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.TrxNilaiHarianDao;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.model.TrxNilaiHarianDetailModel;
import com.xsis.bootcamp.model.TrxNilaiHarianModel;

@Repository
public class TrxNilaiHarianDaoImpl implements TrxNilaiHarianDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<TrxNilaiHarianModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TrxNilaiHarianModel> result = session.createQuery("select nil from TrxNilaiHarianModel as nil").list();
		return result;
	}

	@Override
	public void insert(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public TrxNilaiHarianModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TrxNilaiHarianModel result = session.get(TrxNilaiHarianModel.class, id);
        return result;
	}

	@Override
	public void update(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		String hql = "delete from TrxNilaiHarianDetailModel where trxNilaiId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		if(model.getDetailNilai().size()>0){
			for (TrxNilaiHarianDetailModel item : model.getDetailNilai()){
				item.setTrxNilaiId(model.getId());
				
				session.save(item);
			}
		}
	}

	@Override
	public void delete(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "delete from TrxNilaiHarianDetailModel where trxNilaiId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		session.delete(model);

	}

	@Override
	public void save(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		List<TrxNilaiHarianDetailModel> items = model.getDetailNilai();
		if(items.size()>0){
			for (TrxNilaiHarianDetailModel item : items) {
				item.setTrxNilaiId(model.getId());
				session.save(item);
			}
		}
		
	
	}

}
