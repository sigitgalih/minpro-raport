package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.UsersRoleDao;
import com.xsis.bootcamp.model.UsersRoleModel;

@Repository
public class UsersRoleDaoImpl implements UsersRoleDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<UsersRoleModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<UsersRoleModel> result = session.createQuery("select rol from UsersRoleModel as rol").list();
		return result;
	}

	@Override
	public void insert(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public UsersRoleModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		UsersRoleModel result = session.get(UsersRoleModel.class, id);
        return result;
	}

	@Override
	public void update(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public int getByNamaRole(String roleName) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "select role from UsersRoleModel as role where role.roleName = :roleName";
		Query query = session.createQuery(hql);
		query.setParameter("roleName", roleName);
		int result = 0;
		if (query.list().size() > 0) {
			result = 1;
		}
		return result;
	}
	
	
}
