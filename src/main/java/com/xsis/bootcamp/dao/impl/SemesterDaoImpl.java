package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.SemesterDao;
import com.xsis.bootcamp.model.SemesterModel;
import com.xsis.bootcamp.model.UsersModel;

@Repository
public class SemesterDaoImpl implements SemesterDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<SemesterModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<SemesterModel> result = session.createQuery("select sem from SemesterModel as sem").list();
		return result;
	}

	@Override
	public void insert(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public SemesterModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		SemesterModel result = session.get(SemesterModel.class, id);
        return result;
	}

	@Override
	public void update(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}
	
	
}
