package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.EkskulDao;
import com.xsis.bootcamp.model.EkskulModel;

@Repository
public class EkskulDaoImpl implements EkskulDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<EkskulModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<EkskulModel> result = session.createQuery("select eks from EkskulModel as eks").list();
		return result;
	}

	@Override
	public void insert(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public EkskulModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		EkskulModel result = session.get(EkskulModel.class, id);
        return result;
	}

	@Override
	public void update(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public int getByNamaEkskul(String nmEkskul) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "select eks from EkskulModel as eks where eks.nmEkskul = :nmEkskul";
		Query query = session.createQuery(hql);
		query.setParameter("nmEkskul", nmEkskul);
		int result = 0;
		 if (query.list().size() > 0) {
			result = 1;
		}
		 return result;
	}

}
