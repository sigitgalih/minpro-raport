package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.MapelKelasDao;
import com.xsis.bootcamp.model.MapelKelasModel;

@Repository
public class MapelKelasDaoImpl implements MapelKelasDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<MapelKelasModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MapelKelasModel> result = session.createQuery("select sis from MapelKelasModel as sis").list();
		return result;
	}

	@Override
	public void insert(MapelKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public MapelKelasModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MapelKelasModel result = session.get(MapelKelasModel.class, id);
        return result;
	}

	@Override
	public void update(MapelKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(MapelKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(MapelKelasModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	

}
