package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.StatusAbsenDao;
import com.xsis.bootcamp.model.StatusAbsenModel;
import com.xsis.bootcamp.model.UsersRoleModel;

@Repository
public class StatusAbsenDaoImp implements StatusAbsenDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<StatusAbsenModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<StatusAbsenModel> result = session.createQuery("select status from StatusAbsenModel as status").list();
		return result;
	}

	@Override
	public void insert(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public StatusAbsenModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		StatusAbsenModel result = session.get(StatusAbsenModel.class, id);
        return result;
	}

	@Override
	public void update(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public int getByNamaStatus(String keterangan) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "select status from StatusAbsenModel as status where status.keterangan = :keterangan";
		Query query = session.createQuery(hql);
		query.setParameter("keterangan", keterangan);
		int result = 0;
		if (query.list().size() > 0) {
			result = 1;
		}
		return result;
	}
}
