package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.TrxKelasDetailDao;
import com.xsis.bootcamp.model.TrxKelasDetailModel;

@Repository
public class TrxKelasDetailDaoImpl implements TrxKelasDetailDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TrxKelasDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TrxKelasDetailModel> result = session.createQuery("select trx from TrxKelasDetailModel as trx").list();
		return result;
	}

	@Override
	public void insert(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public TrxKelasDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TrxKelasDetailModel result = session.get(TrxKelasDetailModel.class, id);
        return result;
	}

	@Override
	public void update(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

}
