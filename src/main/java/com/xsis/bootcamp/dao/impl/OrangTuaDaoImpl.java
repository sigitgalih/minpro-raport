package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.OrangTuaDao;
import com.xsis.bootcamp.model.OrangTuaModel;

@Repository
public class OrangTuaDaoImpl implements OrangTuaDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<OrangTuaModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<OrangTuaModel> result = session.createQuery("select ortu from OrangTuaModel as ortu").list();
		return result;
	}

	@Override
	public void insert(OrangTuaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public OrangTuaModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		OrangTuaModel result = session.get(OrangTuaModel.class, id);
        return result;
	}

	@Override
	public void update(OrangTuaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(OrangTuaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(OrangTuaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	

}
