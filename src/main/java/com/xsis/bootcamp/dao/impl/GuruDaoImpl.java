package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.GuruDao;
import com.xsis.bootcamp.model.GuruModel;

@Repository
public class GuruDaoImpl implements GuruDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<GuruModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<GuruModel> result = session.createQuery("select gur from GuruModel as gur").list();
		return result;
	}

	@Override
	public void insert(GuruModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public GuruModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		GuruModel result = session.get(GuruModel.class, id);
        return result;
	}

	@Override
	public void update(GuruModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(GuruModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(GuruModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	

}
