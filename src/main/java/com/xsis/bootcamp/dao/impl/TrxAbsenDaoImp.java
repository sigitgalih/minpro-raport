package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.TrxAbsenDao;
import com.xsis.bootcamp.model.TrxAbsenDetailModel;
import com.xsis.bootcamp.model.TrxAbsenModel;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.model.TrxNilaiHarianDetailModel;

@Repository
public class TrxAbsenDaoImp implements TrxAbsenDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TrxAbsenModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TrxAbsenModel> result = session.createQuery("select status from TrxAbsenModel as status").list();
		return result;
	}

	@Override
	public void insert(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public TrxAbsenModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TrxAbsenModel result = session.get(TrxAbsenModel.class, id);
        return result;
	}

	@Override
	public void update(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		// untuk update di detail
		String hql = "delete from TrxAbsenDetailModel where trxAbsenId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		if(model.getAbsenDetail().size()>0){
			for (TrxAbsenDetailModel item : model.getAbsenDetail()){
				item.setTrxAbsenId(model.getId());
				
				session.save(item);
			}
		}
		
	}

	@Override
	public void delete(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "delete from TrxAbsenDetailModel where trxAbsenId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		session.delete(model);
	}

	@Override
	public void save(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<TrxAbsenDetailModel> items = model.getAbsenDetail();
		if(items.size()>0){
			for (TrxAbsenDetailModel item : items) {
				item.setTrxAbsenId(model.getId());
				session.save(item);
			}
		}
	}

	@Override
	public int getByGuru(int guruId) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "select gur from TrxAbsenModel as gur where gur.guruId = :guruId";
		Query query = session.createQuery(hql);
		query.setParameter("guruId", guruId);
		int result=0;
		if (query.list().size() > 0) {
			result = 1;
		}
		return result;
	}

	

	
	
	
}
