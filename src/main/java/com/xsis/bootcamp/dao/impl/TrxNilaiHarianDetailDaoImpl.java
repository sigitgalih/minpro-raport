package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.TrxNilaiHarianDetailDao;
import com.xsis.bootcamp.model.TrxNilaiHarianDetailModel;

@Repository
public class TrxNilaiHarianDetailDaoImpl implements TrxNilaiHarianDetailDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TrxNilaiHarianDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TrxNilaiHarianDetailModel> result = session.createQuery("select trx from TrxNilaiHarianDetailModel as trx").list();
		return result;
	}

	@Override
	public void insert(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public TrxNilaiHarianDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TrxNilaiHarianDetailModel result = session.get(TrxNilaiHarianDetailModel.class, id);
        return result;
	}

	@Override
	public void update(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

}
