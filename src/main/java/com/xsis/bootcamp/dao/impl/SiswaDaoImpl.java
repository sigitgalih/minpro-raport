package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.SiswaDao;
import com.xsis.bootcamp.model.SiswaModel;
import com.xsis.bootcamp.model.TrxKelasModel;

@Repository
public class SiswaDaoImpl implements SiswaDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<SiswaModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<SiswaModel> result = session.createQuery("select sis from SiswaModel as sis").list();
		return result;
	}

	@Override
	public void insert(SiswaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public SiswaModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		SiswaModel result = session.get(SiswaModel.class, id);
        return result;
	}
	
	@Override
	public List<SiswaModel> getBytrxKelasId(int id) throws Exception{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(SiswaModel.class);
		criteria.add(Restrictions.eq("trxKelasId", id));
		List<SiswaModel> result = criteria.list();
        return result;
	}

	@Override
	public void update(SiswaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(SiswaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}
	
	@Override
	public void save(SiswaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	
	@Override
	public int getByNisn(String nisn) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "select x from SiswaModel as x where x.nisn = :nisn";
		Query query = session.createQuery(hql);
		query.setParameter("nisn", nisn);
		int result=0;
		if(query.list().size()>0){
			result=1;
		}
		return result;
	}

}
