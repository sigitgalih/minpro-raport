package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.MapelDao;
import com.xsis.bootcamp.model.MapelModel;

@Repository
public class MapelDaoImpl implements MapelDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<MapelModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MapelModel> result = session.createQuery("select map from MapelModel as map").list();
		return result;
	}

	@Override
	public void insert(MapelModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public MapelModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MapelModel result = session.get(MapelModel.class, id);
        return result;
	}

	@Override
	public void update(MapelModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(MapelModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(MapelModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	

}
