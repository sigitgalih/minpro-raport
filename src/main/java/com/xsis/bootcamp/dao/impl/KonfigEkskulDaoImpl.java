package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.KonfigEkskulDao;
import com.xsis.bootcamp.model.KonfigEkskulDetailModel;
import com.xsis.bootcamp.model.KonfigEkskulModel;
import com.xsis.bootcamp.model.TrxKelasDetailModel;

@Repository
public class KonfigEkskulDaoImpl implements KonfigEkskulDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<KonfigEkskulModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KonfigEkskulModel> result = session.createQuery("select konf from KonfigEkskulModel as konf").list();
		return result;
	}

	@Override
	public void insert(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public KonfigEkskulModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		KonfigEkskulModel result = session.get(KonfigEkskulModel.class, id);
        return result;
	}

	@Override
	public void update(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
		String hql = "delete from KonfigEkskulDetailModel where konfigEkskulId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		if(model.getKonfigEkskulDetail().size()>0){
			for (KonfigEkskulDetailModel item : model.getKonfigEkskulDetail()){
				item.setKonfigEkskulId(model.getId());
				session.save(item);
			}
		}
	}

	@Override
	public void delete(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "delete from KonfigEkskulDetailModel where konfigEkskulId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		query.executeUpdate();
		
		session.delete(model);
	}

	@Override
	public void save(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<KonfigEkskulDetailModel> items = model.getKonfigEkskulDetail();
		if(items.size()>0){
			for (KonfigEkskulDetailModel item : items) {
				item.setKonfigEkskulId(model.getId());
				session.save(item);
			}
		}

	}

}
