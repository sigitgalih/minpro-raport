package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.KelasDao;
import com.xsis.bootcamp.model.KelasModel;
import com.xsis.bootcamp.model.UsersRoleModel;

@Repository
public class KelasDaoImpl implements KelasDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<KelasModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KelasModel> result = session.createQuery("select kel from KelasModel as kel").list();
		return result;
	}

	@Override
	public void insert(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public KelasModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		KelasModel result = session.get(KelasModel.class, id);
        return result;
	}
	
	
	@Override
	public void update(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public void save(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public int getByNama(String nama) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
	
		
		String hql = "select x from KelasModel as x where x.nama = :nama";
		Query query = session.createQuery(hql);
		query.setParameter("nama", nama);
		int result = 0;
		if (query.list().size() > 0) {
			result = 1;
		}
		return result;
	}
}
