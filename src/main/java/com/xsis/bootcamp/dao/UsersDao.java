package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.UsersModel;

public interface UsersDao {
	public List<UsersModel> get() throws Exception;
	public void insert(UsersModel model) throws Exception;
	public UsersModel getById(int id) throws Exception;
	public void update(UsersModel model) throws Exception;
	public void delete(UsersModel model) throws Exception;
	public void save(UsersModel model) throws Exception;
}
