package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.TrxAbsenModel;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.model.TrxKelasModel;

public interface TrxAbsenDao {
	public List<TrxAbsenModel> get() throws Exception;
	public void insert(TrxAbsenModel model) throws Exception;
	public TrxAbsenModel getById(int id) throws Exception;
	public int getByGuru(int guruId) throws Exception;
	public void update(TrxAbsenModel model) throws Exception;
	public void delete(TrxAbsenModel model) throws Exception;
	public void save(TrxAbsenModel model) throws Exception;
}
