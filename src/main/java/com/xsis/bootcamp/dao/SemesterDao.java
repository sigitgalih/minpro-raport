package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.SemesterModel;

public interface SemesterDao {
	public List<SemesterModel> get() throws Exception;
	public void insert(SemesterModel model) throws Exception;
	public SemesterModel getById(int id) throws Exception;
	public void update(SemesterModel model) throws Exception;
	public void delete(SemesterModel model) throws Exception;
	public void save(SemesterModel model) throws Exception;
}
