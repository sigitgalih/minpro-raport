package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.MapelModel;

public interface MapelDao {
	public List<MapelModel> get() throws Exception;
	public void insert(MapelModel model) throws Exception;
	public MapelModel getById(int id) throws Exception;
	public void update(MapelModel model) throws Exception;
	public void delete(MapelModel model) throws Exception;
	public void save(MapelModel model) throws Exception;
}
