package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.GuruModel;

public interface GuruDao {
	public List<GuruModel> get() throws Exception;
	public void insert(GuruModel model) throws Exception;
	public GuruModel getById(int id) throws Exception;
	public void update(GuruModel model) throws Exception;
	public void delete(GuruModel model) throws Exception;
	public void save(GuruModel model) throws Exception;
}
