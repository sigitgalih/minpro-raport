package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.TrxKelasModel;

public interface TrxKelasDao {
	public List<TrxKelasModel> get() throws Exception;
	public void insert(TrxKelasModel model) throws Exception;
	public TrxKelasModel getById(int id) throws Exception;/*
	public List<TrxKelasModel> getByIdKelas() throws Exception;*/
	public void update(TrxKelasModel model) throws Exception;
	public void delete(TrxKelasModel model) throws Exception;
	public void save(TrxKelasModel model) throws Exception;
}
