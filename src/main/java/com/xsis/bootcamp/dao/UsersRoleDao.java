package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.UsersRoleModel;

public interface UsersRoleDao {
	public List<UsersRoleModel> get() throws Exception;
	public void insert(UsersRoleModel model) throws Exception;
	public UsersRoleModel getById(int id) throws Exception;
	public int getByNamaRole(String roleName) throws Exception;
	public void update(UsersRoleModel model) throws Exception;
	public void delete(UsersRoleModel model) throws Exception;
	public void save(UsersRoleModel model) throws Exception;
}
