package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.dao.StatusAbsenDao;
import com.xsis.bootcamp.model.StatusAbsenModel;

public interface StatusAbsenService extends StatusAbsenDao {
	public List<StatusAbsenModel> get() throws Exception;
	public void insert(StatusAbsenModel model) throws Exception;
	public StatusAbsenModel getById(int id) throws Exception;
	public int getByNamaStatus(String ketarangan) throws Exception;
	public void update(StatusAbsenModel model) throws Exception;
	public void delete(StatusAbsenModel model) throws Exception;
	public void save(StatusAbsenModel model) throws Exception;
}
