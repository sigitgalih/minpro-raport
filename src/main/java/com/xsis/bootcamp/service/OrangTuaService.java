package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.OrangTuaModel;

public interface OrangTuaService {
	public List<OrangTuaModel> get() throws Exception;
	public void insert(OrangTuaModel model) throws Exception;
	public OrangTuaModel getById(int id) throws Exception;
	public void update(OrangTuaModel model) throws Exception;
	public void delete(OrangTuaModel model) throws Exception;
	public void save(OrangTuaModel model) throws Exception;
}
