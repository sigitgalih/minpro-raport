package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.dao.KelasDao;
import com.xsis.bootcamp.model.KelasModel;

public interface KelasService extends KelasDao {
	public List<KelasModel> get() throws Exception;
	public void insert(KelasModel model) throws Exception;
	public KelasModel getById(int id) throws Exception;
	public int getByNama(String nama) throws Exception;
	public void update(KelasModel model) throws Exception;
	public void delete(KelasModel model) throws Exception;
	public void save(KelasModel model) throws Exception;
}
