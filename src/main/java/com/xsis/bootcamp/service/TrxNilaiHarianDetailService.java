package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.TrxNilaiHarianDetailModel;

public interface TrxNilaiHarianDetailService {
	public List<TrxNilaiHarianDetailModel> get() throws Exception;
	public void insert(TrxNilaiHarianDetailModel model) throws Exception;
	public TrxNilaiHarianDetailModel getById(int id) throws Exception;
	public void update(TrxNilaiHarianDetailModel model) throws Exception;
	public void delete(TrxNilaiHarianDetailModel model) throws Exception;
	public void save(TrxNilaiHarianDetailModel model) throws Exception;
}
