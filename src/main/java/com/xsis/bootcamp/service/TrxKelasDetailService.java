package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.TrxKelasDetailModel;

public interface TrxKelasDetailService {
	public List<TrxKelasDetailModel> get() throws Exception;
	public void insert(TrxKelasDetailModel model) throws Exception;
	public TrxKelasDetailModel getById(int id) throws Exception;
	public void update(TrxKelasDetailModel model) throws Exception;
	public void delete(TrxKelasDetailModel model) throws Exception;
	public void save(TrxKelasDetailModel model) throws Exception;
}
