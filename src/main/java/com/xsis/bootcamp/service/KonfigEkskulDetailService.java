package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.KonfigEkskulDetailModel;

public interface KonfigEkskulDetailService {
	public List<KonfigEkskulDetailModel> get() throws Exception;
	public void insert(KonfigEkskulDetailModel model) throws Exception;
	public KonfigEkskulDetailModel getById(int id) throws Exception;
	public void update(KonfigEkskulDetailModel model) throws Exception;
	public void delete(KonfigEkskulDetailModel model) throws Exception;
	public void save(KonfigEkskulDetailModel model) throws Exception;
}
