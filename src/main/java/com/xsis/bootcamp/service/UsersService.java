package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.UsersModel;

public interface UsersService {
	public List<UsersModel> get() throws Exception;
	public void insert(UsersModel model) throws Exception;
	public UsersModel getById(int id) throws Exception;
	public void update(UsersModel model) throws Exception;
	public void delete(UsersModel model) throws Exception;
	public void save(UsersModel model) throws Exception;
}
