package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.KonfigEkskulModel;

public interface KonfigEkskulService {
	public List<KonfigEkskulModel> get() throws Exception;
	public void insert(KonfigEkskulModel model) throws Exception;
	public KonfigEkskulModel getById(int id) throws Exception;
	public void update(KonfigEkskulModel model) throws Exception;
	public void delete(KonfigEkskulModel model) throws Exception;
	public void save(KonfigEkskulModel model) throws Exception;
}
