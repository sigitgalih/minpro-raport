package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.TAModel;

public interface TAService {
	public List<TAModel> get() throws Exception;
	public void insert(TAModel model) throws Exception;
	public TAModel getById(int id) throws Exception;
	public void update(TAModel model) throws Exception;
	public void delete(TAModel model) throws Exception;
	public void save(TAModel model) throws Exception;
}
