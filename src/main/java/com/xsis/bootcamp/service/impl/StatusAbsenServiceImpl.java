package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.StatusAbsenDao;
import com.xsis.bootcamp.model.StatusAbsenModel;
import com.xsis.bootcamp.service.StatusAbsenService;

@Service
@Transactional
public class StatusAbsenServiceImpl implements StatusAbsenService {
	@Autowired
    private StatusAbsenDao dao;
	
	@Override
	public List<StatusAbsenModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public StatusAbsenModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(StatusAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

	@Override
	public int getByNamaStatus(String keterangan) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByNamaStatus(keterangan);
	}
	
}
