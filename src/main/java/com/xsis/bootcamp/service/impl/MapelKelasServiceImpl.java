package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.MapelKelasDao;
import com.xsis.bootcamp.model.MapelKelasModel;
import com.xsis.bootcamp.service.MapelKelasService;

@Service
@Transactional
public class MapelKelasServiceImpl implements MapelKelasService {
    @Autowired
    private MapelKelasDao dao;
	
	@Override
	public List<MapelKelasModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(MapelKelasModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public MapelKelasModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(MapelKelasModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(MapelKelasModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(MapelKelasModel model) throws Exception {
		this.dao.save(model);
		
	}
}
