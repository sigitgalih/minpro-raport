package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.MapelDao;
import com.xsis.bootcamp.model.MapelModel;
import com.xsis.bootcamp.service.MapelService;

@Service
@Transactional
public class MapelServiceImpl implements MapelService {
    @Autowired
    private MapelDao dao;
	
	@Override
	public List<MapelModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(MapelModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public MapelModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(MapelModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(MapelModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(MapelModel model) throws Exception {
		this.dao.save(model);
		
	}
}
