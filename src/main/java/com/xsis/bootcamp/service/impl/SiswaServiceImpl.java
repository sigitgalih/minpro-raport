package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.SiswaDao;
import com.xsis.bootcamp.model.SiswaModel;
import com.xsis.bootcamp.service.SiswaService;

@Service
@Transactional
public class SiswaServiceImpl implements SiswaService {
    @Autowired
    private SiswaDao dao;
	
	@Override
	public List<SiswaModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(SiswaModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public SiswaModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(SiswaModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(SiswaModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(SiswaModel model) throws Exception {
		this.dao.save(model);
		
	}
	
	@Override
	public List<SiswaModel> getBytrxKelasId(int id) throws Exception {
		return this.dao.getBytrxKelasId(id);
	}
	
	@Override
	public int getByNisn(String nisn) throws Exception{
		return this.dao.getByNisn(nisn);
	}
}
