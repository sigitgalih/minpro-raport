package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.SemesterDao;
import com.xsis.bootcamp.model.SemesterModel;
import com.xsis.bootcamp.service.SemesterService;

@Service
@Transactional
public class SemesterServiceImpl implements SemesterService{
	@Autowired
    private SemesterDao dao;
	
	@Override
	public List<SemesterModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public SemesterModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(SemesterModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

}
