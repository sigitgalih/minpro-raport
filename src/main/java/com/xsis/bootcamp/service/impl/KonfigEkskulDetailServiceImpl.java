package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.KonfigEkskulDetailDao;
import com.xsis.bootcamp.dao.KonfigEkskulDetailDao;
import com.xsis.bootcamp.model.KonfigEkskulDetailModel;
import com.xsis.bootcamp.model.KonfigEkskulDetailModel;
import com.xsis.bootcamp.service.KonfigEkskulDetailService;

@Service
@Transactional
public class KonfigEkskulDetailServiceImpl implements KonfigEkskulDetailService {
	@Autowired
    private KonfigEkskulDetailDao dao;
	
	@Override
	public List<KonfigEkskulDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public KonfigEkskulDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(KonfigEkskulDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

}
