package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.EkskulDao;
import com.xsis.bootcamp.model.EkskulModel;
import com.xsis.bootcamp.service.EkskulService;

@Service
@Transactional
public class EkskulServiceImpl implements EkskulService{
	@Autowired
    private EkskulDao dao;
	
	@Override
	public List<EkskulModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public EkskulModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(EkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

	@Override
	public int getByNamaEkskul(String nmEkskul) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByNamaEkskul(nmEkskul);
	}
	
}
