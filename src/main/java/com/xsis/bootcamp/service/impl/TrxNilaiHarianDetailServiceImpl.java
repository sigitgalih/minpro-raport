package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.TrxNilaiHarianDetailDao;
import com.xsis.bootcamp.model.TrxNilaiHarianDetailModel;
import com.xsis.bootcamp.service.TrxNilaiHarianDetailService;

@Service
@Transactional
public class TrxNilaiHarianDetailServiceImpl implements TrxNilaiHarianDetailService {
	@Autowired
    private TrxNilaiHarianDetailDao dao;
	
	@Override
	public List<TrxNilaiHarianDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public TrxNilaiHarianDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(TrxNilaiHarianDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

}
