package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.UsersDao;
import com.xsis.bootcamp.model.UsersModel;
import com.xsis.bootcamp.service.UsersService;

@Service
@Transactional
public class UsersServiceImpl implements UsersService {
	@Autowired
    private UsersDao dao;
	
	@Override
	public List<UsersModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(UsersModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public UsersModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(UsersModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(UsersModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(UsersModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}
	
}
