package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.TrxNilaiHarianDao;
import com.xsis.bootcamp.model.TrxNilaiHarianModel;
import com.xsis.bootcamp.service.TrxNilaiHarianService;

@Service
@Transactional
public class TrxNilaiHarianServiceImpl implements TrxNilaiHarianService {
	@Autowired
	private TrxNilaiHarianDao dao;
	 
	@Override
	public List<TrxNilaiHarianModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public TrxNilaiHarianModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(TrxNilaiHarianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}
	
}
