package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.KonfigEkskulDao;
import com.xsis.bootcamp.model.KonfigEkskulModel;
import com.xsis.bootcamp.service.KonfigEkskulService;

@Service
@Transactional
public class KonfigEkskulServiceImpl implements KonfigEkskulService {
	@Autowired
    private KonfigEkskulDao dao;
	
	@Override
	public List<KonfigEkskulModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public KonfigEkskulModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(KonfigEkskulModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

}
