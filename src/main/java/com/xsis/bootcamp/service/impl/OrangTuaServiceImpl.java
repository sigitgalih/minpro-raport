package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.OrangTuaDao;
import com.xsis.bootcamp.model.OrangTuaModel;
import com.xsis.bootcamp.service.OrangTuaService;

@Service
@Transactional
public class OrangTuaServiceImpl implements OrangTuaService {
    @Autowired
    private OrangTuaDao dao;
	
	@Override
	public List<OrangTuaModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(OrangTuaModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public OrangTuaModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(OrangTuaModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(OrangTuaModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(OrangTuaModel model) throws Exception {
		this.dao.save(model);
		
	}
}
