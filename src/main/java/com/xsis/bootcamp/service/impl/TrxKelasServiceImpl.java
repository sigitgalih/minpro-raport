package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.TrxKelasDao;
import com.xsis.bootcamp.model.TrxKelasModel;
import com.xsis.bootcamp.service.TrxKelasService;

@Service
@Transactional
public class TrxKelasServiceImpl implements TrxKelasService {
    @Autowired
    private TrxKelasDao dao;
	
	@Override
	public List<TrxKelasModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(TrxKelasModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public TrxKelasModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(TrxKelasModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(TrxKelasModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(TrxKelasModel model) throws Exception {
		this.dao.save(model);
		
	}
}
