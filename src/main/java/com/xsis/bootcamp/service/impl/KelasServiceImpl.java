package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.KelasDao;
import com.xsis.bootcamp.model.KelasModel;
import com.xsis.bootcamp.service.KelasService;

@Service
@Transactional
public class KelasServiceImpl implements KelasService {
	@Autowired
    private KelasDao dao;
	
	@Override
	public List<KelasModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public KelasModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(KelasModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

	@Override
	public int getByNama(String nama) throws Exception {
		return this.dao.getByNama(nama);
	}
}
