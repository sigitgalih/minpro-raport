package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.TADao;
import com.xsis.bootcamp.model.TAModel;
import com.xsis.bootcamp.service.TAService;

@Service
@Transactional
public class TAServiceImpl implements TAService{
	@Autowired
    private TADao dao;

	@Override
	public List<TAModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(TAModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public TAModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(TAModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(TAModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(TAModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}
	
	
}
