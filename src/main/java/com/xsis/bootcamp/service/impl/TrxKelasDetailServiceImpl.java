package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.TrxKelasDetailDao;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.service.TrxKelasDetailService;

@Service
@Transactional
public class TrxKelasDetailServiceImpl implements TrxKelasDetailService {
	@Autowired
    private TrxKelasDetailDao dao;
	
	@Override
	public List<TrxKelasDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public TrxKelasDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(TrxKelasDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

}
