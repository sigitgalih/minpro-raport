package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.GuruDao;
import com.xsis.bootcamp.model.GuruModel;
import com.xsis.bootcamp.service.GuruService;

@Service
@Transactional
public class GuruServiceImpl implements GuruService {
    @Autowired
    private GuruDao dao;
	
	@Override
	public List<GuruModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(GuruModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public GuruModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(GuruModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(GuruModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(GuruModel model) throws Exception {
		this.dao.save(model);
		
	}
}
