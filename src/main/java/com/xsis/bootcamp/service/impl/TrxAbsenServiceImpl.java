package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.TrxAbsenDao;
import com.xsis.bootcamp.model.TrxAbsenModel;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.service.TrxAbsenService;

@Service
@Transactional
public class TrxAbsenServiceImpl implements TrxAbsenService {
	@Autowired
    private TrxAbsenDao dao;
	
	@Override
	public List<TrxAbsenModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public TrxAbsenModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(TrxAbsenModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

	@Override
	public int getByGuru(int guruId) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByGuru(guruId);
	}



	

	

	


	
}
