package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.UsersRoleDao;
import com.xsis.bootcamp.model.UsersRoleModel;
import com.xsis.bootcamp.service.UsersRoleService;

@Service
@Transactional
public class UsersRoleServiceImpl implements UsersRoleService {
	@Autowired
    private UsersRoleDao dao;
	
	@Override
	public List<UsersRoleModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public UsersRoleModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public void save(UsersRoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.save(model);
	}

	@Override
	public int getByNamaRole(String roleName) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByNamaRole(roleName);
	}
	
}
