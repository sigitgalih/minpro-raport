package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.MapelKelasModel;

public interface MapelKelasService {
	public List<MapelKelasModel> get() throws Exception;
	public void insert(MapelKelasModel model) throws Exception;
	public MapelKelasModel getById(int id) throws Exception;
	public void update(MapelKelasModel model) throws Exception;
	public void delete(MapelKelasModel model) throws Exception;
	public void save(MapelKelasModel model) throws Exception;
}
