package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.SiswaModel;

public interface SiswaService {
	public List<SiswaModel> get() throws Exception;
	public void insert(SiswaModel model) throws Exception;
	public SiswaModel getById(int id) throws Exception;
	public void update(SiswaModel model) throws Exception;
	public void delete(SiswaModel model) throws Exception;
	public void save(SiswaModel model) throws Exception;
	public List<SiswaModel> getBytrxKelasId(int id) throws Exception;
	public int getByNisn(String nisn) throws Exception;
}
