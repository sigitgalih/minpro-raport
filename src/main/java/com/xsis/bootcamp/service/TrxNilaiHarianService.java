package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.TrxNilaiHarianModel;

public interface TrxNilaiHarianService {
	public List<TrxNilaiHarianModel> get() throws Exception;
	public void insert(TrxNilaiHarianModel model) throws Exception;
	public TrxNilaiHarianModel getById(int id) throws Exception;
	public void update(TrxNilaiHarianModel model) throws Exception;
	public void delete(TrxNilaiHarianModel model) throws Exception;
	public void save(TrxNilaiHarianModel model) throws Exception;
}
