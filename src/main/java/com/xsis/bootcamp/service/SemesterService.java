package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.dao.SemesterDao;
import com.xsis.bootcamp.model.SemesterModel;

public interface SemesterService extends SemesterDao {
	public List<SemesterModel> get() throws Exception;
	public void insert(SemesterModel model) throws Exception;
	public SemesterModel getById(int id) throws Exception;
	public void update(SemesterModel model) throws Exception;
	public void delete(SemesterModel model) throws Exception;
	public void save(SemesterModel model) throws Exception;
}
