package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "MAPELKELAS")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class MapelKelasModel {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "MAPELKELAS")
	@TableGenerator(name = "MAPELKELAS", table = "SEQUENCE", pkColumnName = "ID", pkColumnValue = "MAPELKELAS", valueColumnName = "VALUE", allocationSize = 1, initialValue = 1)
	private int id;

	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private MapelModel mapel;
	
	public MapelModel getMapel() {
		return mapel;
	}

	public void setMapel(MapelModel mapel) {
		this.mapel = mapel;
	}

	// relelasi dari model ke suplier dengan mengambil data suplier
	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private KelasModel kelas;

	public KelasModel getKelas() {
		return kelas;
	}

	public void setKelas(KelasModel kelas) {
		this.kelas = kelas;
	}

	@Column(name = "KELAS_ID")
	private Integer kelasId;
	@Column(name = "MAPEL_ID")
	private Integer mapelId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getKelasId() {
		return kelasId;
	}

	public void setKelasId(Integer kelasId) {
		this.kelasId = kelasId;
	}

	public Integer getMapelId() {
		return mapelId;
	}

	public void setMapelId(Integer mapelId) {
		this.mapelId = mapelId;
	}

}
