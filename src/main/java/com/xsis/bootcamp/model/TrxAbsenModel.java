package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TRX_ABSEN")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TrxAbsenModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TRX_ABSEN")
	@TableGenerator(name="TRX_ABSEN",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TRX_ABSEN", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_ABSEN")
	private Date tglAbsen;

	

	@Column(name="ABSEN_ID")
	private Integer absenId;
	@Column(name="TOTAL_ABSEN")
	private Integer totalAbsen;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="trxAbsenId")
	private List<TrxAbsenDetailModel> absenDetail;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public Integer getAbsenId() {
		return absenId;
	}

	public void setAbsenId(Integer absenId) {
		this.absenId = absenId;
	}

	public Integer getTotalAbsen() {
		return totalAbsen;
	}

	public void setTotalAbsen(Integer totalAbsen) {
		this.totalAbsen = totalAbsen;
	}

	public List<TrxAbsenDetailModel> getAbsenDetail() {
		return absenDetail;
	}

	public void setAbsenDetail(List<TrxAbsenDetailModel> absenDetail) {
		this.absenDetail = absenDetail;
	}
	
	public Date getTglAbsen() {
		return tglAbsen;
	}

	public void setTglAbsen(Date tglAbsen) {
		this.tglAbsen = tglAbsen;
	}



	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ABSEN_ID", insertable=false, updatable=false)
	private StatusAbsenModel absen;

	public StatusAbsenModel getAbsen() {
		return absen;
	}

	public void setAbsen(StatusAbsenModel absen) {
		this.absen = absen;
	}
	
}
