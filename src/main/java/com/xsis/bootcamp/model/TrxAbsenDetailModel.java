package com.xsis.bootcamp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TRX_ABSEN_DETAIL")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TrxAbsenDetailModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TRX_ABSEN_DETAIL")
	@TableGenerator(name="TRX_ABSEN_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TRX_ABSEN_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	//	@Column(name="TGL_NILAI")
//	private Date tglNilai;
	

	@Column(name="TRX_ABSEN_ID")
	private Integer trxAbsenId;
	
	@Column(name="ABSEN")
	private Integer absen;
	
	@Column(name="SISWA_ID")
	private Integer siswaId;
	
	@Column(name="TRX_KELAS_ID")
	private Integer trxKelasId;


	public Integer getTrxKelasId() {
		return trxKelasId;
	}

	public void setTrxKelasId(Integer trxKelasId) {
		this.trxKelasId = trxKelasId;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="trxAbsenId", insertable=false, updatable=false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public Integer getTrxAbsenId() {
		return trxAbsenId;
	}

	public void setTrxAbsenId(Integer trxAbsenId) {
		this.trxAbsenId = trxAbsenId;
	}

	public Integer getAbsen() {
		return absen;
	}

	public void setAbsen(Integer absen) {
		this.absen = absen;
	}

	public Integer getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Integer siswaId) {
		this.siswaId = siswaId;
	}
	
	// ======= join table siswa absen ========
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SISWA_ID", insertable=false, updatable=false)
	private SiswaModel siswa;

	public SiswaModel getSiswa() {
		return siswa;
	}

	public void setSiswa(SiswaModel siswa) {
		this.siswa = siswa;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="TRX_KELAS_ID", insertable=false, updatable=false)
	private TrxKelasModel kelas;


	public TrxKelasModel getKelas() {
		return kelas;
	}

	public void setKelas(TrxKelasModel kelas) {
		this.kelas = kelas;
	}
	

}
