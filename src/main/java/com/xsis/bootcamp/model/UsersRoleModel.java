package com.xsis.bootcamp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="USERS_ROLE")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class UsersRoleModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="USERS_ROLE")
	@TableGenerator(name="USERS_ROLE",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="USERS_ROLE", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="ROLE_NAME")
	private String roleName;
	
	//@OneToMany(mappedBy="usersRole", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	/*private UsersModel users;

	public UsersModel getUsers() {
		return users;
	}

	public void setUsers(UsersModel users) {
		this.users = users;
	}*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}
