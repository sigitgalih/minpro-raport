package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="USERS")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UsersModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="USERS")
	@TableGenerator(name="USERS",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="USERS", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="USERNAMES")
	private String usernames;
	
	@Column(name="PASS")
	private String pass;
	
	@Column(name="ROLE_ID")
	private int roleId;
	
	@Column(name="ACTIVATE")
	private int activate;

	public int getActivate() {
		return activate;
	}

	public void setActivate(int activate) {
		this.activate = activate;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private UsersRoleModel usersRole;
	
/*	public UsersRoleModel getUsersRole() {
		return usersRole;
	}

	public void setUsersRole(UsersRoleModel usersRole) {
		this.usersRole = usersRole;
	}*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsernames() {
		return usernames;
	}

	public void setUsernames(String usernames) {
		this.usernames = usernames;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	
}
