package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="SEMESTER")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class SemesterModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="SEMESTER")
	@TableGenerator(name="SEMESTER",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="SEMESTER", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NM_SEMESTER")
	private String nmSemester;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNmSemester() {
		return nmSemester;
	}

	public void setNmSemester(String nmSemester) {
		this.nmSemester = nmSemester;
	}
	
	
}
