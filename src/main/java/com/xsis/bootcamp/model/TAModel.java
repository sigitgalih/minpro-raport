package com.xsis.bootcamp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TA")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TAModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TA")
	@TableGenerator(name="TA",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TA", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="TAHUN_AJARAN")
	private String tahunAjaran;
	
	@DateTimeFormat(pattern="mm/dd/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_MULAI")
	private Date tglMulai;
	
	@DateTimeFormat(pattern="mm/dd/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_SELESAI")
	private Date tglSelesai;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public Date getTglMulai() {
		return tglMulai;
	}

	public void setTglMulai(Date tglMulai) {
		this.tglMulai = tglMulai;
	}

	public Date getTglSelesai() {
		return tglSelesai;
	}

	public void setTglSelesai(Date tglSelesai) {
		this.tglSelesai = tglSelesai;
	}
	
	
}
