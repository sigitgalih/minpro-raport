package com.xsis.bootcamp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TRX_KELAS_DETAIL")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TrxKelasDetailModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TRX_KELAS_DETAIL")
	@TableGenerator(name="TRX_KELAS_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TRX_KELAS_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	@Column(name="TRX_KELAS_ID")
	private Integer trxKelasId;
	
	@Column(name="SISWA_ID")
	private Integer siswaId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="trxKelasId", insertable=false, updatable=false)
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getTrxKelasId() {
		return trxKelasId;
	}

	public void setTrxKelasId(Integer trxKelasId) {
		this.trxKelasId = trxKelasId;
	}

	public Integer getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Integer siswaId) {
		this.siswaId = siswaId;
	}		
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SISWA_ID", insertable=false, updatable=false)
	private SiswaModel siswa;

	public SiswaModel getSiswa() {
		return siswa;
	}

	public void setSiswa(SiswaModel siswa) {
		this.siswa = siswa;
	}
	

}
