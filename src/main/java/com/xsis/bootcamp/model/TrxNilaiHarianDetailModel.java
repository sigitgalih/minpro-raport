package com.xsis.bootcamp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TRX_NILAI_HARIAN_DETAIL")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TrxNilaiHarianDetailModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TRX_NILAI_HARIAN_DETAIL")
	@TableGenerator(name="TRX_NILAI_HARIAN_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TRX_NILAI_HARIAN_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="TRX_NILAI_ID")
	private Integer trxNilaiId;

	@Column(name="NILAI")
	private Integer nilai;
	
	@Column(name="SISWA_ID")
	private Integer siswaId;
	
	@Column(name="TRX_KELAS_ID")
	private Integer trxKelasId;
	
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="trxNilaiId", insertable=false, updatable=false)
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getTrxNilaiId() {
		return trxNilaiId;
	}
	public void setTrxNilaiId(Integer trxNilaiId) {
		this.trxNilaiId = trxNilaiId;
	}
	
	public Integer getSiswaId() {
		return siswaId;
	}
	public void setSiswaId(Integer siswaId) {
		this.siswaId = siswaId;
	}
	
	public Integer getNilai() {
		return nilai;
	}
	public void setNilai(Integer nilai) {
		this.nilai = nilai;
	}
	public Integer getTrxKelasId() {
		return trxKelasId;
	}
	public void setTrxKelasId(Integer trxKelasId) {
		this.trxKelasId = trxKelasId;
	}
	
	
	//========== join table siswa =============
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SISWA_ID", insertable=false, updatable=false)
	private SiswaModel siswa;

	public SiswaModel getSiswa() {
		return siswa;
	}
	public void setSiswa(SiswaModel siswa) {
		this.siswa = siswa;
	}
	
	//=============join table TRX KELAS==============
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="TRX_KELAS_ID", insertable=false, updatable=false)
	private TrxKelasModel kelas;
	
	public TrxKelasModel getKelas() {
		return kelas;
	}
	public void setKelas(TrxKelasModel kelas) {
		this.kelas = kelas;
	}
}
