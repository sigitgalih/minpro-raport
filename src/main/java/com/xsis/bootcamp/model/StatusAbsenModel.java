package com.xsis.bootcamp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="STATUSABSEN")

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class StatusAbsenModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="STATUSABSEN")
	@TableGenerator(name="STATUSABSEN",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="STATUSABSEN", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	/*@OneToOne(mappedBy= "STATUSABSEN", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private StatusAbsenModel statusAbsen;
	


	public StatusAbsenModel getStatusAbsen() {
		return statusAbsen;
	}

	public void setStatusAbsen(StatusAbsenModel statusAbsen) {
		this.statusAbsen = statusAbsen;
	}*/

	@Column(name="KETERANGAN")
	private String keterangan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getketerangan() {
		return keterangan;
	}

	public void setketerangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
}
