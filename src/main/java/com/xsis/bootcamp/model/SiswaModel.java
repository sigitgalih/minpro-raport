package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="SISWA")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")

public class SiswaModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="SISWA")
	@TableGenerator(name="SISWA",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="SISWA", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NISN")
	private String nisn;
	@Column(name="NAMA_SISWA")
	private String nmSiswa;
	@Column(name="JK")
	private String jk;
	@Column (name="TEMPAT")
	private String tempat;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_LAHIR")
	private Date tglLahir;
	@Column(name="ALAMAT")
	private String alamat;
	@Column(name="TELP")
	private String noTelp;
	@Column(name="JML_SAUDARA")
	private Integer jmlSaudara;
	
	@Column(name="NAMA_AYAH")
	private String nmAyah;
	@Column(name="NO_TELP_AYAH")
	private String noTelpAyah;
	@Column(name="PEKERJAAN_AYAH")
	private String pekerjaanAyah;
	@Column(name="ALAMAT_AYAH")
	private String alamatAyah;
	
	@Column(name="NAMA_IBU")
	private String nmIbu;
	@Column(name="NO_TELP_IBU")
	private String noTelpIbu;
	@Column(name="PEKERJAAN_IBU")
	private String pekerjaanIbu;
	@Column(name="ALAMAT_IBU")
	private String alamatIbu;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	} 
	public String getNisn() {
		return nisn;
	}
	public void setNisn(String nisn) {
		this.nisn = nisn;
	}
	
	public String getNmSiswa() {
		return nmSiswa;
	}
	public void setNmSiswa(String nmSiswa) {
		this.nmSiswa = nmSiswa;
	}
	public String getJk() {
		return jk;
	}
	public void setJk(String jk) {
		this.jk = jk;
	}
	public String getTempat() {
		return tempat;
	}
	public void setTempat(String tempat) {
		this.tempat = tempat;
	}
	public Date getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public Integer getJmlSaudara() {
		return jmlSaudara;
	}
	public void setJmlSaudara(Integer jmlSaudara) {
		this.jmlSaudara = jmlSaudara;
	}
	public String getNmAyah() {
		return nmAyah;
	}
	public void setNmAyah(String nmAyah) {
		this.nmAyah = nmAyah;
	}
	public String getNmIbu() {
		return nmIbu;
	}
	public void setNmIbu(String nmIbu) {
		this.nmIbu = nmIbu;
	}
	public String getPekerjaanAyah() {
		return pekerjaanAyah;
	}
	public void setPekerjaanAyah(String pekerjaanAyah) {
		this.pekerjaanAyah = pekerjaanAyah;
	}
	public String getAlamatAyah() {
		return alamatAyah;
	}
	public void setAlamatAyah(String alamatAyah) {
		this.alamatAyah = alamatAyah;
	}
	
	public String getPekerjaanIbu() {
		return pekerjaanIbu;
	}
	public void setPekerjaanIbu(String pekerjaanIbu) {
		this.pekerjaanIbu = pekerjaanIbu;
	}
	public String getAlamatIbu() {
		return alamatIbu;
	}
	public void setAlamatIbu(String alamatIbu) {
		this.alamatIbu = alamatIbu;
	}
	public String getNoTelpAyah() {
		return noTelpAyah;
	}
	public void setNoTelpAyah(String noTelpAyah) {
		this.noTelpAyah = noTelpAyah;
	}
	public String getNoTelpIbu() {
		return noTelpIbu;
	}
	public void setNoTelpIbu(String noTelpIbu) {
		this.noTelpIbu = noTelpIbu;
	}
	
}
