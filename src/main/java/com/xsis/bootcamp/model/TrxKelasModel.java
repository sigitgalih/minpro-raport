package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TRX_KELAS")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TrxKelasModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TRX_KELAS")
	@TableGenerator(name="TRX_KELAS",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TRX_KELAS", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="trxKelasId")
	private List<TrxKelasDetailModel> detail;
	
	public List<TrxKelasDetailModel> getTrxKelasDetail() {
		return detail;
	}

	public void setTrxKelasDetail(List<TrxKelasDetailModel> detail) {
		this.detail =detail;
	}

	@Column(name="KELAS_ID")
	private Integer kelasId;
	
	@Column(name="GURU_ID")
	private Integer guruId;
	
	@Column(name="TA_ID")
	private Integer taId;

	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Integer getKelasId() {
		return kelasId;
	}


	public void setKelasId(Integer kelasId) {
		this.kelasId = kelasId;
	}


	public Integer getGuruId() {
		return guruId;
	}


	public void setGuruId(Integer guruId) {
		this.guruId = guruId;
	}


	public Integer getTaId() {
		return taId;
	}


	public void setTaId(Integer taId) {
		this.taId = taId;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="KELAS_ID", insertable=false, updatable=false)
	private KelasModel kelas;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="GURU_ID", insertable=false, updatable=false)
	private GuruModel guru;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="TA_ID", insertable=false, updatable=false)
	private TAModel ta;

	public KelasModel getKelas() {
		return kelas;
	}

	public void setKelas(KelasModel kelas) {
		this.kelas = kelas;
	}


	public List<TrxKelasDetailModel> getDetail() {
		return detail;
	}

	public void setDetail(List<TrxKelasDetailModel> detail) {
		this.detail = detail;
	}

	public GuruModel getGuru() {
		return guru;
	}

	public void setGuru(GuruModel guru) {
		this.guru = guru;
	}

	public TAModel getTa() {
		return ta;
	}

	public void setTa(TAModel ta) {
		this.ta = ta;
	}
	
		
}
