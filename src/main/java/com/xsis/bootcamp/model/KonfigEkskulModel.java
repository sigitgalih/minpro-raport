package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="KONFIG_EKSKUL")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")

public class KonfigEkskulModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="KONFIG_EKSKUL")
	@TableGenerator(name="KONFIG_EKSKUL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="KONFIG_EKSKUL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="konfigEkskulId")
	private List<KonfigEkskulDetailModel> konfigEkskulDetail;
	

	public List<KonfigEkskulDetailModel> getKonfigEkskulDetail() {
		return konfigEkskulDetail;
	}

	public void setKonfigEkskulDetail(List<KonfigEkskulDetailModel> konfigEkskulDetail) {
		this.konfigEkskulDetail = konfigEkskulDetail;
	}
	
	
	@Column(name="EKSKUL_ID")
	private Integer ekskulId;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_EKSKUL")
	private Date tglEkskul;
	
	@Column(name="GURU_ID")
	private Integer guruId;
	
	@Column(name="TA_ID")
	private Integer taId;
	
	@Column(name="SEMESTER_ID")
	private Integer semesterId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getEkskulId() {
		return ekskulId;
	}

	public void setEkskulId(Integer ekskulId) {
		this.ekskulId = ekskulId;
	}
	
	public Date getTglEkskul() {
		return tglEkskul;
	}

	public void setTglEkskul(Date tglEkskul) {
		this.tglEkskul = tglEkskul;
	}

	public Integer getGuruId() {
		return guruId;
	}

	public void setGuruId(Integer guruId) {
		this.guruId = guruId;
	}

	public Integer getTaId() {
		return taId;
	}

	public void setTaId(Integer taId) {
		this.taId = taId;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="EKSKUL_ID", insertable=false, updatable=false)
	private EkskulModel ekskul;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="GURU_ID", insertable=false, updatable=false)
	private GuruModel guru;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="TA_ID", insertable=false, updatable=false)
	private TAModel ta;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SEMESTER_ID", insertable=false, updatable=false)
	private SemesterModel semester;

	public EkskulModel getEkskul() {
		return ekskul;
	}

	public void setEkskul(EkskulModel ekskul) {
		this.ekskul = ekskul;
	}

	public GuruModel getGuru() {
		return guru;
	}

	public void setGuru(GuruModel guru) {
		this.guru = guru;
	}

	public TAModel getTa() {
		return ta;
	}

	public void setTa(TAModel ta) {
		this.ta = ta;
	}

	public SemesterModel getSemester() {
		return semester;
	}

	public void setSemester(SemesterModel semester) {
		this.semester = semester;
	}
	
}
