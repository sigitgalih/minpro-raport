package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="KONFIG_EKSKUL_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class KonfigEkskulDetailModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="KONFIG_EKSKUL_DETAIL")
	@TableGenerator(name="KONFIG_EKSKUL_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="KONFIG_EKSKUL_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	
	@Column(name="KONFIG_EKSKUL_ID")
	private Integer konfigEkskulId;
	
	@Column(name="SISWA_ID")
	private Integer siswaId;
	
	@Column(name="NILAI")
	private Integer nilai;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="konfigEkskulId", insertable=false, updatable=false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getKonfigEkskulId() {
		return konfigEkskulId;
	}

	public void setKonfigEkskulId(Integer konfigEkskulId) {
		this.konfigEkskulId = konfigEkskulId;
	}

	public Integer getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Integer siswaId) {
		this.siswaId = siswaId;
	}
	
	public Integer getNilai() {
		return nilai;
	}

	public void setNilai(Integer nilai) {
		this.nilai = nilai;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SISWA_ID", insertable=false, updatable=false)
	private SiswaModel siswa;

	public SiswaModel getSiswa() {
		return siswa;
	}

	public void setSiswa(SiswaModel siswa) {
		this.siswa = siswa;
	}
	
}
