package com.xsis.bootcamp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="MAPEL")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")

public class MapelModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="MAPEL")
	@TableGenerator(name="MAPEL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="MAPEL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@OneToOne(mappedBy= "mapel", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private MapelKelasModel mapelKelas;
	
	public MapelKelasModel getMapel() {
		return mapelKelas;
	}
	public void setMapel(MapelKelasModel mapelKelas) {
		this.mapelKelas = mapelKelas;
	}
	@Column(name="KD_MAPEL")
	private Integer kdMapel;
	@Column(name="NM_MAPEL")
	private String nmMapel;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getkdMapel() {
		return kdMapel;
	}
	public void setkdMapel(Integer kdMapel) {
		this.kdMapel = kdMapel;
	}
	public String getNmMapel() {
		return nmMapel;
	}
	public void setNmMapel(String nmMapel) {
		this.nmMapel = nmMapel;
	}
	
	
}
