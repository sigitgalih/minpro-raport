package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="EKSKUL")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class EkskulModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="EKSKUL")
	@TableGenerator(name="EKSKUL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="EKSKUL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NAMA_EKSKUL")
	private String nmEkskul;
	
	@Column(name="DESKRIPSI")
	private String deskripsi;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNmEkskul() {
		return nmEkskul;
	}

	public void setNmEkskul(String nmEkskul) {
		this.nmEkskul = nmEkskul;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
}
