package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="TRX_NILAI_HARIAN_MODEL")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class TrxNilaiHarianModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TRX_NILAI_HARIAN_MODEL")
	@TableGenerator(name="TRX_NILAI_HARIAN_MODEL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="TRX_NILAI_HARIAN_MODEL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_NILAI")
	private Date tglNilai;
	
	@Column(name="SEMESTER_ID")
	private Integer semesterId;
	
	@Column(name="MAPEL_ID")
	private Integer mapelId;
	
	@Column(name="NILAI_TOTAL")
	private Integer totNilai;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="trxNilaiId")
	private List<TrxNilaiHarianDetailModel> detailNilai;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTglNilai() {
		return tglNilai;
	}

	public void setTglNilai(Date tglNilai) {
		this.tglNilai = tglNilai;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public Integer getMapelId() {
		return mapelId;
	}

	public void setMapelId(Integer mapelId) {
		this.mapelId = mapelId;
	}

	public Integer getTotNilai() {
		return totNilai;
	}

	public void setTotNilai(Integer totNilai) {
		this.totNilai = totNilai;
	}

	public List<TrxNilaiHarianDetailModel> getDetailNilai() {
		return detailNilai;
	}

	public void setDetailNilai(List<TrxNilaiHarianDetailModel> detailNilai) {
		this.detailNilai = detailNilai;
	}

	//============ join ke table ===================
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MAPEL_ID", insertable=false, updatable=false)
	private MapelModel mapel;
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SEMESTER_ID", insertable=false, updatable=false)
	private SemesterModel semester;
	
	public MapelModel getMapel() {
		return mapel;
	}

	public void setMapel(MapelModel mapel) {
		this.mapel = mapel;
	}

	public SemesterModel getSemester() {
		return semester;
	}

	public void setSemester(SemesterModel semester) {
		this.semester = semester;
	}
}
