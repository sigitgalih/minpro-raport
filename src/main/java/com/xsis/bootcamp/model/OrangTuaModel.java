package com.xsis.bootcamp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "ORANGTUA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class OrangTuaModel {

	// property ID
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ORANGTUA")
	@TableGenerator(name = "ORANGTUA", table = "SEQUENCE", pkColumnName = "ID", pkColumnValue = "ORANGTUA", valueColumnName = "VALUE", allocationSize = 1, initialValue = 1)
	private int id;

	@Column(name = "NAMA_AYAH")
	private String nmAyah;

	@Column(name = "NAMA_IBU")
	private String nmIbu;

	@Column(name = "ALAMAT")
	private String alamat;

	@Column(name = "SISWA_ID")
	private Integer siswaId;
	@Column(name = "NO_HP")
	private String noTelpon;
	@Column(name = "USER_ID")
	private Integer userId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNmAyah() {
		return nmAyah;
	}
	public void setNmAyah(String nmAyah) {
		this.nmAyah = nmAyah;
	}
	public String getNmIbu() {
		return nmIbu;
	}
	public void setNmIbu(String nmIbu) {
		this.nmIbu = nmIbu;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Integer getSiswaId() {
		return siswaId;
	}
	public void setSiswaId(Integer siswaId) {
		this.siswaId = siswaId;
	}
	public String getNoTelpon() {
		return noTelpon;
	}
	public void setNoTelpon(String noTelpon) {
		this.noTelpon = noTelpon;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	

}
