package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.EkskulModel;
import com.xsis.bootcamp.service.EkskulService;

@Controller
public class EkskulController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private EkskulService service;
	
	@RequestMapping(value = "/ekskul")
	public String index() {
		return "ekskul";
	}

	@RequestMapping(value = "/ekskul/save")
	public String save(Model model, @ModelAttribute EkskulModel item, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "ekskul/save";
	}

	@RequestMapping(value = "/ekskul/load")
	public String load(Model model) {
		List<EkskulModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "ekskul/load";
	}
	
	@RequestMapping(value = "/ekskul/getById")
	public String getById(Model model, HttpServletRequest request) {
		EkskulModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "ekskul/getById";
	}
	
	@RequestMapping(value = "/ekskul/getByNamaEkskul")
	public String getByNama(Model model, HttpServletRequest request){
		int data = 0;
		String nmEkskul = request.getParameter("nmEkskul");
		String result = "";
		try {
			data = this.service.getByNamaEkskul(nmEkskul);
			result = "success";
		} catch (Exception e) {
			// TODO: handle exception
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		model.addAttribute("data", data);
		model.addAttribute("result", result);
		return "ekskul/getByNamaEkskul";
	}

}
