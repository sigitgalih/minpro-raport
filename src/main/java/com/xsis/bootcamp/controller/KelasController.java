package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.KelasModel;
import com.xsis.bootcamp.service.KelasService;

@Controller
public class KelasController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private KelasService service;

	@RequestMapping(value = "/kelas")
	public String index() {
		return "kelas";
	}

	@RequestMapping(value = "/kelas/save")
	public String save(Model model, @ModelAttribute KelasModel item, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "kelas/save";
	}

	@RequestMapping(value = "/kelas/load")
	public String load(Model model) {
		List<KelasModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "kelas/load";
	}
	
	@RequestMapping(value = "/kelas/getById")
	public String getById(Model model, HttpServletRequest request) {
		KelasModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "kelas/getById";
	}
	
	@RequestMapping(value = "/kelas/getByNama")
	public String getByNama(Model model, HttpServletRequest request) {
		int data = 0;
		String nama = request.getParameter("nama");
		String result = "";
		try {
			data = this.service.getByNama(nama);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "kelas/getByNama";
	}

}
