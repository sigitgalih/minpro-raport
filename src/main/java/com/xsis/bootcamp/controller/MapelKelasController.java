package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.MapelKelasModel;
import com.xsis.bootcamp.service.MapelKelasService;

@Controller
public class MapelKelasController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private MapelKelasService service;

	@RequestMapping(value = "/mapelKelas")
	public String index() {
		return "mapelKelas";
	}

	@RequestMapping(value = "/mapelKelas/save")
	public String save(Model model, @ModelAttribute MapelKelasModel item, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "mapelKelas/save";
	}

	@RequestMapping(value = "/mapelKelas/load")
	public String load(Model model) {
		List<MapelKelasModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "mapelKelas/load";
	}
	
	@RequestMapping(value = "/mapelKelas/getById")
	public String getById(Model model, HttpServletRequest request) {
		MapelKelasModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "mapelKelas/getById";
	}

}
