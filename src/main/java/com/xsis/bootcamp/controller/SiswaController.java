package com.xsis.bootcamp.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.SiswaModel;
import com.xsis.bootcamp.service.SiswaService;

@Controller
public class SiswaController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private SiswaService service;

	@RequestMapping(value = "/siswa")
	public String index() { 
		return "siswa";
	}

	@RequestMapping(value = "/siswa/save")
	public String save(Model model, @ModelAttribute SiswaModel item,BindingResult binding, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "siswa/save";
	}

	@RequestMapping(value = "/siswa/load")
	public String load(Model model) {
		List<SiswaModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "siswa/load";
	}
	
	@RequestMapping(value = "/siswa/getById")
	public String getById(Model model, HttpServletRequest request) {
		SiswaModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "siswa/getById";
	}
	
	
	
	@RequestMapping(value="/siswa/listBytrxKelas")
	public String listBytrxKelas(Model model, HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));
		// membuat object list dari class Kabupaten model
		List<SiswaModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.getBytrxKelasId(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "siswa/listBytrxKelas";
	}
		
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}	
}