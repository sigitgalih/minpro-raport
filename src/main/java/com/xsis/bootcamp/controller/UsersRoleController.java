package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.UsersRoleModel;
import com.xsis.bootcamp.service.UsersRoleService;

@Controller
public class UsersRoleController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private UsersRoleService service;

	@RequestMapping(value = "/usersRole")
	public String index() {
		return "usersRole";
	}

	@RequestMapping(value = "/usersRole/save")
	public String save(Model model, @ModelAttribute UsersRoleModel item, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "usersRole/save";
	}

	@RequestMapping(value = "/usersRole/load")
	public String load(Model model) {
		List<UsersRoleModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "usersRole/load";
	}
	
	@RequestMapping(value = "/usersRole/getById")
	public String getById(Model model, HttpServletRequest request) {
		UsersRoleModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "usersRole/getById";
	}
	
	@RequestMapping(value="/usersRole/getByNamaRole")
	public String getByNamaRole(Model model, HttpServletRequest request){
		int data=0;
		String roleName = request.getParameter("roleName");
		String result="";
		try {
			data = this.service.getByNamaRole(roleName);
			result = "success";
		} catch (Exception e) {
			result="gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);
		
		return "usersRole/getByNamaRole";
	}

}
