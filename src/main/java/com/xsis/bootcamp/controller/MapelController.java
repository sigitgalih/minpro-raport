package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.MapelModel;
import com.xsis.bootcamp.service.MapelService;

@Controller
public class MapelController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private MapelService service;

	@RequestMapping(value = "/mapel")
	public String index() {
		return "mapel";
	}

	@RequestMapping(value = "/mapel/save")
	public String save(Model model, @ModelAttribute MapelModel item, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "mapel/save";
	}

	@RequestMapping(value = "/mapel/load")
	public String load(Model model) {
		List<MapelModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "mapel/load";
	}
	
	@RequestMapping(value = "/mapel/getById")
	public String getById(Model model, HttpServletRequest request) {
		MapelModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "mapel/getById";
	}
}