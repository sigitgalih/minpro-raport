package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.xsis.bootcamp.model.StatusAbsenModel;
import com.xsis.bootcamp.service.StatusAbsenService;

@Controller
public class StatusAbsenController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private StatusAbsenService service;

	@RequestMapping(value = "/statusAbsen")
	public String index() {
		return "statusAbsen";
	}

	@RequestMapping(value = "/statusAbsen/save")
	public String save(Model model, @ModelAttribute StatusAbsenModel item, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "statusAbsen/save";
	}

	@RequestMapping(value = "/statusAbsen/load")
	public String load(Model model) {
		List<StatusAbsenModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "statusAbsen/load";
	}
	
	@RequestMapping(value = "/statusAbsen/getById")
	public String getById(Model model, HttpServletRequest request) {
		StatusAbsenModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "statusAbsen/getById";
	}
	
	// ============ get by nama status =======
	@RequestMapping(value="statusAbsen/getByNamaStatus")
	public String getByNamaStatus(Model model, HttpServletRequest request){
		int data = 0;
		String keterangan = request.getParameter("keterangan");
		String result = "";
		try {
			data = this.service.getByNamaStatus(keterangan);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		model.addAttribute("data", data);
		model.addAttribute("result", result);
		return "statusAbsen/getByNamaStatus";
	}

}
