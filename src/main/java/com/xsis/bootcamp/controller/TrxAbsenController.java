package com.xsis.bootcamp.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.TrxAbsenModel;
import com.xsis.bootcamp.model.TrxKelasDetailModel;
import com.xsis.bootcamp.model.TrxKelasModel;
import com.xsis.bootcamp.service.TrxAbsenService;

@Controller
public class TrxAbsenController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private TrxAbsenService service;

	@RequestMapping(value = "/trxAbsen")
	public String index() {
		return "trxAbsen";
	}

	@RequestMapping(value = "/trxAbsen/save")
	public String save(Model model, @ModelAttribute TrxAbsenModel item, BindingResult binding,
			HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");

		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "trxAbsen/save";
	}

	@RequestMapping(value = "/trxAbsen/load")
	public String load(Model model) {
		List<TrxAbsenModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "trxAbsen/load";
	}

	@RequestMapping(value = "/trxAbsen/getById")
	public String getById(Model model, HttpServletRequest request) {
		TrxAbsenModel data = null;
		int id = Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "trxAbsen/getById";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	// ===============list jstl =============
	@RequestMapping(value = "/trxAbsen/list")
	public String list(Model model) {
		List<TrxAbsenModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "trxAbsen/list";
	}

	// ============== validation get by siswa false =======
	@RequestMapping(value = "/trxAbsen/getByGuru")
	public String getByGuru(Model model, HttpServletRequest request) {
		int data = 1;
		int guruId = Integer.parseInt(request.getParameter("guruId"));
		String result = "";
		try {
			data = this.service.getByGuru(guruId);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "trxAbsen/getByGuru";
	}
}
