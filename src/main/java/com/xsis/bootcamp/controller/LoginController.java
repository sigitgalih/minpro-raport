package com.xsis.bootcamp.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value="error", required=false) String error,
			@RequestParam(value="logout",required=false) String logout){
		ModelAndView model = new ModelAndView();
		if(error != null){
			model.addObject("error","Invalid username and password");
		}
		if(logout != null){
			model.addObject("msg","You are have logged out successfully");
		}
		return model;
	}
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public ModelAndView index(){
		return new ModelAndView("/index");
	}
	
	@RequestMapping(value="/select-outlet", method=RequestMethod.GET)
	public ModelAndView selectOutlet(){
		return new ModelAndView("/select-outlet");
	}
	
	@RequestMapping(value="/403", method=RequestMethod.GET)
	public ModelAndView AccessDenied(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth instanceof AnonymousAuthenticationToken)){
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}
		return model;
	}
}
