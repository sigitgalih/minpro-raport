package com.xsis.bootcamp.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.GuruModel;
import com.xsis.bootcamp.service.GuruService;

@Controller
public class GuruController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private GuruService service;

	@RequestMapping(value = "/guru")
	public String index() {
		return "guru";
	}

	@RequestMapping(value = "/guru/save")
	public String save(Model model, @ModelAttribute GuruModel item,BindingResult binding, HttpServletRequest request) {
		String result = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "guru/save";
	}

	@RequestMapping(value = "/guru/load")
	public String load(Model model) {
		List<GuruModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "guru/load";
	}
	
	@RequestMapping(value="/guru/list")
	public String list (Model model){
		List<GuruModel> data = null;
		String result="";
		try {
			data = this.service.get();
			result="success";
		} catch (Exception e) {
			result="gagal";
			log.error(e.getMessage(), e);
		}
		model.addAttribute("data", data);
		model.addAttribute("result", result);
		return "guru/list";
	}
	
	@RequestMapping(value = "/guru/getById")
	public String getById(Model model, HttpServletRequest request) {
		GuruModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "guru/getById";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}	
}