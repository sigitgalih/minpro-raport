package com.xsis.bootcamp.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.TrxAbsenModel;
import com.xsis.bootcamp.model.TrxNilaiHarianDetailModel;
import com.xsis.bootcamp.model.TrxNilaiHarianModel;
import com.xsis.bootcamp.service.TrxNilaiHarianService;

@Controller
public class TrxNilaiHarianController {
private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private TrxNilaiHarianService service;

	@RequestMapping(value = "/trxNilaiHarian")
	public String index() {
		return "trxNilaiHarian";
	}

	@RequestMapping(value = "/trxNilaiHarian/save")
	public String save(Model model, @ModelAttribute TrxNilaiHarianModel item, BindingResult binding, TrxNilaiHarianDetailModel items, HttpServletRequest request) {
		String result = "";
		String result2 = "";
		String proses = request.getParameter("proses");
		
		try {
			if (proses.equals("insert")) {
				this.service.save(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else {
				this.service.delete(item);
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "trxNilaiHarian/save";
	}

	@RequestMapping(value = "/trxNilaiHarian/load")
	public String load(Model model) {
		List<TrxNilaiHarianModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "trxNilaiHarian/load";
	}
	
	@RequestMapping(value = "/trxNilaiHarian/getById")
	public String getById(Model model, HttpServletRequest request) {
		TrxNilaiHarianModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "trxNilaiHarian/getById";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	// ===============list jstl =============
		@RequestMapping(value = "/trxNilaiHarian/list")
		public String list(Model model) {
			List<TrxNilaiHarianModel> data = null;
			String result = "";
			try {
				data = this.service.get();
				result = "success";

			} catch (Exception e) {
				result = "gagal";
				log.error(e.getMessage(), e);
			}

			model.addAttribute("data", data);
			model.addAttribute("result", result);

			return "trxNilaiHarian/list";
		}

}
